#include <stdio.h>
#include <stdlib.h>

#define EXIT 2
enum flag{STRING,SUBSTRING,STRING_SUBSTRING};


void clbuf()
{
	while(getchar()!='\n');
}

void getint(int *ref,int min,int max)
{
	printf("Enter your choice:");
	while(!(scanf("%d",ref)) || (*ref<min || *ref>max))
	{
	        clbuf();
		printf("Invalid input\nEnter your choice: ");
	} 
	clbuf();
}

char* strn_cpy(char* dest,char* src,int num)
{
	int index;
	if(num)
	{
      		for (index = 0; index < num ; index++)
		{
			if(*(src+index)=='\0')
				break;
        		*(dest+index) = *(src+index);
		}
		*(dest+index)='\0';
    	}
        return dest;
}

char* str_str(char *s1, char *s2)
{
        int index1=0,index2=0;
 	while (s1[index1]!='\0')     
	{                
		while (s2[index2]!='\0' && s1[index2+index1]==s2[index2])    
		{                                                                         
			index2++;						
		}
		if(s2[index2]=='\0')    
			return s1+index1;
		index1++; 
		index2=0;
	}
	return 0;
}

int str_len(char *str)
{
	if(str==NULL)
		return 0;
    	int index;
    	for(index=0;str[index]!='\0';index++);
    	return index;
}

char* getinput()
{
	int len=0;
	char *cptr=NULL;
	char ch;
        cptr=(char*) malloc(sizeof(char));
	if(cptr==NULL)
        {
		printf("No memory allocated");
		return 0;
	}
        while(scanf("%c",&ch) && ch!='^')
	{
		cptr[len]=ch;   
		len++; 
		cptr=(char*) realloc(cptr,sizeof(char)*len+1);                                               
	} 
	clbuf();
	cptr[len]='\0';
	return cptr; 
}

int find_string(char* input,char* find_str,char *replace_str, int flag)
{
	char *result=NULL,*temp_result,*temp,*temp_input;
	int find_len=str_len(find_str),strcount=0,substrcount=0,len=0,replace_len,null_check=0;
	temp_input=input;
	if(flag>=0)
	{
		result=(char*) malloc(1*sizeof(char));
		if(result==NULL)
		{
			printf("No memory allocated");
			return 0;
		}
		*result='\0';
		temp_result=result;
		replace_len=str_len(replace_str);
	}
	while(1)
	{
		temp = temp_input;
        	temp_input = str_str(temp_input, find_str);
		if(temp_input==NULL)
		{
			if(flag>=0)
				strn_cpy(temp_result, temp,str_len(temp)); 
			break;
		}
		len+=temp_input-temp;
		if(flag>=0)
		{
			result=(char*) realloc(result,(len+1)*sizeof(char));
			if(result==NULL)
			{
				printf("No memory allocated");
				return 0;
			}
			temp_result=result; 
			if((null_check++)!=0)
				temp_result+=str_len(result); 
			strn_cpy(temp_result,temp,temp_input-temp);
			temp_result=temp_result+(temp_input-temp);		
		} 
		if((*(temp_input+find_len)=='\t'|| *(temp_input+find_len)==' ' || *(temp_input+find_len)=='\0') && (*(temp_input-1)==' ' || *(temp_input+find_len)=='\t' || temp_input==input))
		{
			strcount++;
			if(flag==STRING || flag==STRING_SUBSTRING)
			{
				len+=replace_len;
				result=realloc(result,(len+1)*sizeof(char));
				temp_result=result; 
				temp_result=temp_result+len-replace_len;
				strn_cpy(temp_result,replace_str,replace_len);
				temp_result=temp_result+replace_len;
			} 
			else if(flag==SUBSTRING)
			{
				len+=find_len;
				result=realloc(result,(len+1)*sizeof(char));
				temp_result=result; 
				temp_result=temp_result+len-find_len;
				strn_cpy(temp_result,find_str,find_len);
				temp_result=temp_result+find_len;
			}
		}
		else
		{
			substrcount++;
		        if(flag==SUBSTRING || flag==STRING_SUBSTRING)
			{
				len+=replace_len;
				result=realloc(result,(len+1)*sizeof(char));
				temp_result=result; 
				temp_result=temp_result+len-replace_len;
				strn_cpy(temp_result,replace_str,replace_len);
				temp_result=temp_result+replace_len;
			} 
			else if(flag==STRING)
			{
				len+=find_len;
				result=realloc(result,(len+1)*sizeof(char));
				temp_result=result; 
				temp_result=temp_result+len-find_len;
				strn_cpy(temp_result,find_str,find_len);
				temp_result=temp_result+find_len;
			} 
		} 
		temp_input=temp_input+find_len;
	}
	if(flag>=0)
	{
		printf("The string is:\n %s",result);
		if(result)
		{
			free(result);
			result=NULL;
		}
	}	
	else
		printf("The string count is:%d\nThe substring count is:%d",strcount,substrcount);
	if(strcount==0 && substrcount==0)
		return 0;
	else
		return 1;
		
}

int main()
{
	int choice,min=1,max,flag,findmenu_flag=1,value=0;
	char *input=NULL,*find_str=NULL,*replace_string=NULL;
MENU:   printf("Enter the input string:");
    	input=getinput();
	do {
	         printf("\nMENU:\n1.Find and replace\n2.Exit\n3.Back\n");
	         max=3;
                 getint(&choice,min,max);
		 switch(choice)
		 {
			case 1:
				do {
               	  		  	printf("\nEnter the string to be searched:");
		  		  	find_str=getinput();
		   			flag=-1;
		   			value=find_string(input,find_str,replace_string,flag);
					if(value)
					{
	 			   		printf("\n1.Replace\n2.Back\n");
						max=3;
		   				getint(&choice,min,max);
						switch(choice)
						{
							case 1:
								printf("\nReplace\n1.String\n2.substring\n3.Both string and substring\n");
								max=3;
                 						getint(&choice,min,max);
								printf("\nEnter the string to be replaced:");
		 						replace_string=getinput();
		 						if(choice==1)
									flag=STRING;
		 						else if(choice==2)
									flag=SUBSTRING;
		 						else
									flag=STRING_SUBSTRING;
		 						value=find_string(input,find_str,replace_string,flag);
		                                	case 2:
		 						findmenu_flag=0;
								break;
						}
					}
					else
					{
						printf("\nNo string found");
						findmenu_flag=0;
					}
					if(find_str)
					{
						free(find_str);
						find_str=NULL;
					}	
					if(replace_string)
					{
						free(replace_string);
						replace_string=NULL;
					}
				}while(findmenu_flag);				
				break;
			case 2:
				if(input)
				{
					free(input);
					input=NULL;
				}
				return 0;
			case 3:
				if(input)
				{
					free(input);
					input=NULL;
				}
				
				goto MENU; 
			default:
				break;
		}   
   	}while(1);
} 





