#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<math.h>

#define MAGIC_ARRAY_SIZE 2
#define NUMBER_OF_COLORS 256
#define COLOR_PALETTE_SIZE 1024
#define RGB_BPP 3

/************************************************************************************************************
  *  Name	:	clbuf
  *  Arguments  :       No arguments
  *  Returns	:	Does not return anything
  *  Description	:   This function is clear buffer and is used to clear input buffer in case of 
                            input buffering.
  ************************************************************************************************************/

void clbuf()
{
	while(getchar()!='\n');
}

/************************************************************************************************************
  *  Name	:	getint
  *  Arguments  :       int pointer,minimum value,maximum value
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to get integer input and to validate them.
  ************************************************************************************************************/

void getint(int *ref,int min,int max)
{
	while(!(scanf("%d",ref)) || (*ref<min || *ref>max))
	{
	        clbuf();
		printf("Invalid input\nEnter a valid num: ");
	} 
	clbuf();
}

/************************************************************************************************************
                            Declaration of array for 'BM' in bmp header
  ************************************************************************************************************/

unsigned char magic[2];

/************************************************************************************************************
                            Declaration of structure for bmp header
  ************************************************************************************************************/

typedef struct
{
    	uint32_t size; 
    	uint16_t reserved1;  
    	uint16_t reserved2;  
    	uint32_t offset; 
}bmpheader;

/************************************************************************************************************
                            Declaration of structure for dib header
  ************************************************************************************************************/

typedef struct 
{
    	uint32_t header_sz; 
    	uint32_t width;  
    	uint32_t height;  
    	uint16_t nplanes; 
    	uint16_t bitcount; 
    	uint32_t compression;
    	uint32_t bmp_bytes;  
    	uint32_t hres;  
    	uint32_t vres;  
    	uint32_t ncolors;  
    	uint32_t nimpcolors; 

}dibheader;

/************************************************************************************************************
  *  Name	:	str_len
  *  Arguments  :       char* string
  *  Returns	:	returns integer
  *  Description	:   This function is used to find the length of the string
  ************************************************************************************************************/

int str_len(char *str)
{
	if(str==NULL)
		return 0;
    	int index;
    	for(index=0;str[index]!='\0';index++);
    	return index;
}

/************************************************************************************************************
  *  Name	:	filecheck_bmp
  *  Arguments  :       char pointer
  *  Returns	:	returns integer
  *  Description	:   This function is used to validate bmp file name
  ************************************************************************************************************/

int filecheck_bmp(char* cptr)
{
	int len=str_len(cptr);
	if(cptr[len-1]=='p' && cptr[len-2]=='m' && cptr[len-3]=='b' && cptr[len-4]=='.')	//check for .bmp file
		return 1;
	else 
		return 0;
}

/************************************************************************************************************
  *  Name	:	load_bmp
  *  Arguments  :       filename,dib header,bmp header
  *  Returns	:	returns the bitmapdata
  *  Description	:   This function is used to load the bmp file.
  ************************************************************************************************************/	

unsigned char *load_bmp(char *filename, dibheader *dib,bmpheader *bmp)
{
    	FILE *fp;
    	unsigned char *bitmapImage=NULL; 
	int size;

    	fp = fopen(filename,"rb");
    	if (fp == NULL)
    	{
		printf("\nFile does not open");	
		fclose(fp);
        	return NULL;
    	}
    	size=fread(&magic,sizeof(char),MAGIC_ARRAY_SIZE,fp);			//read BM from the bmp file header
	if(size!=MAGIC_ARRAY_SIZE)
	{
		printf("\nFile not read correctly");
		fclose(fp);	
        	return NULL;
	}
	if(magic[0]!='B' || magic[1]!='M')
	{
		printf("\nFile not read correctly");
		fclose(fp);	
        	return NULL;
	}

    	size=fread(bmp,1, sizeof(bmpheader),fp);				//read bmp header from file
	if(size!=sizeof(bmpheader))
	{
		printf("\nFile not read correctly");
		fclose(fp);
		return NULL;
	}

    	size=fread(dib,1, sizeof(dibheader),fp); 				//read dibheader from the file
	if(size!=sizeof(dibheader))
	{
		printf("\nFile not read correctly");
		fclose(fp);
		return NULL;
	}

    	fseek(fp, bmp->offset, SEEK_SET);              		      	//move the pointer to the start of image data

	bitmapImage = (unsigned char*)malloc(dib->bmp_bytes);         	//allocate memory for the size of image data
	if(!bitmapImage)
	{
		printf("\nNo memory allocated");
		fclose(fp);
		return NULL;
	}

    	fread(bitmapImage,dib->bmp_bytes,1,fp);				//read bitmapImage data from the file

    	if (bitmapImage == NULL)
    	{
		printf("\nFile not read correctly");
        	fclose(fp);
        	return NULL;
    	}

    	fclose(fp);
    	return bitmapImage;
}

/************************************************************************************************************
  *  Name	:	file_write
  *  Arguments  :       unsinged char buffer
  *  Returns	:	returns integer
  *  Description	:   This function is used to write the output buffer data to the bmp file
  ************************************************************************************************************/

int file_write(bmpheader bmp,dibheader dib,unsigned char* buffer)
{
	int size,i,count=0;
	FILE *fp1=fopen("test.bmp","wb");
	unsigned char* color_palette=NULL;

	if (fp1 == NULL)
    	{
		printf("File does not open");	
        	return 0;
    	}
	
	size=fwrite(magic,sizeof(char),MAGIC_ARRAY_SIZE,fp1);                          //writing BM data into ouput file
	if(size!=MAGIC_ARRAY_SIZE)
	{
		return 0;
	} 

	bmp.size=54+dib.bmp_bytes+COLOR_PALETTE_SIZE;
	bmp.offset=54+COLOR_PALETTE_SIZE;
	dib.bitcount=8;
	dib.ncolors=NUMBER_OF_COLORS;
	dib.hres=0;
	dib.vres=0;
	
	size=fwrite(&bmp,1,sizeof(bmpheader),fp1);			//writing bmp header data into ouput file
	if(size!=sizeof(bmpheader))
	{
		return 0;
	} 
	
	size=fwrite(&dib,1,sizeof(dibheader),fp1);			//writing dib header data into ouput file
	if(size!=sizeof(dibheader))
	{
		return 0;
	}
	
	color_palette=(unsigned char*)malloc(sizeof(unsigned char)*COLOR_PALETTE_SIZE);
	if(!color_palette)
	{
		printf("Memory not allocated");
		return 0;
	}
	
	for(i=0;i<COLOR_PALETTE_SIZE;i+=4)
	{
		color_palette[i]= count;
       		color_palette[i+1]= count;
        	color_palette[i+2]= count;
        	color_palette[i+3]= 0;
		count++;
	}
	
	size=fwrite(color_palette,sizeof(unsigned char),COLOR_PALETTE_SIZE,fp1);
	if(size!=COLOR_PALETTE_SIZE)
	{
		return 0;
	}

	fseek(fp1, bmp.offset, SEEK_SET);				//moving the pointer to start of image data
	
	size=fwrite(buffer,1,dib.bmp_bytes,fp1);			//writing image data into ouput file
	if(size!=dib.bmp_bytes)
	{
		return 0;
	} 

	fclose(fp1);
	if(buffer)
	{
		free(buffer);
		buffer=NULL;
	}
	if(color_palette)
	{
		free(color_palette);
		color_palette=NULL;
	}
	return 1;
}

/************************************************************************************************************
  *  Name	:	rgbToGray
  *  Arguments  :       dibheader pointer,unsigned char data,unsigned char gray
  *  Returns	:	returns unsigned char gray
  *  Description	:   This function is used to convert rgb to grayscale
  ************************************************************************************************************/

unsigned char* rgbToGray(unsigned char* data,unsigned char* gray,dibheader dib)
{
	int row=0,col=0,r,g,b,intensity;
	int width=dib.width;
	int height=dib.height;
	if(height<0)
		height=-height;
	for(row=0;row<height;row++)
	{
		for(col=0;col<width;col++)
		{
			b=data[(width * row + col)*RGB_BPP];
			g=data[(width * row + col)*RGB_BPP+1];
			r=data[(width * row + col)*RGB_BPP+2];
			intensity=(0.3*r)+(0.59*g)+(0.11*b); 				//find intensity of a pixel
			gray[width * row + col]=intensity;				
		}
	}
	return gray;
}

/************************************************************************************************************
  *  Name	:	sobel
  *  Arguments  :       dibheader pointer,unsigned char data,unsigned char result
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to implement sobel algorithm to find the edges
  ************************************************************************************************************/

unsigned char* sobel(unsigned char* data,unsigned char* res,dibheader dib)
{
	int sobelx[3][3] = {-1, 0, 1, -8, 0, 8, -1, 0, 1}, sobely[3][3] = {1, 8, 1, 0, 0, 0, -1, -8, -1};	//defining sobel horizontal(x) and vertical(y) kernel matrix
	int row=0,col=0,dx=0,dy=0,index;
	int width=dib.width;
	int height=dib.height;
	if(height<0)
		height=-height;
	for(col=0;col<width;col++)
	{
		res[col]=data[col];										//writing first column to result
		res[(width*(height-1))+col]=data[(width*(height-1))+col];					//writing last column to result
	}
	for(row=1;row<height-1;row++)
	{
		res[width*row]=data[width*row];									//writing first row to result
		for(col=1;col<width-1;col++)
		{
			dx= data[width*(row-1)+(col-1)]*sobelx[0][0] + data[width*(row-1)+(col)]*sobelx[0][1] + data[width*(row-1)+(col+1)]*sobelx[0][2] 
				+ data[width*(row)+(col-1)]*sobelx[1][0] + data[width*(row)+(col)]*sobelx[1][1] + data[width*(row)+(col+1)]*sobelx[1][2]
				+ data[width*(row+1)+(col-1)]*sobelx[2][0] + data[width*(row+1)+(col)]*sobelx[2][1] + data[width*(row+1)+(col+1)]*sobelx[2][2];

			dy= data[width*(row-1)+(col-1)]*sobely[0][0] + data[width*(row-1)+(col)]*sobely[0][1] + data[width*(row-1)+(col+1)]*sobely[0][2] 
				+ data[width*(row)+(col-1)]*sobely[1][0] + data[width*(row)+(col)]*sobely[1][1] + data[width*(row)+(col+1)]*sobely[1][2]
				+ data[width*(row+1)+(col-1)]*sobely[2][0] + data[width*(row+1)+(col)]*sobely[2][1] + data[width*(row+1)+(col+1)]*sobely[2][2];
			double d=sqrt((dx*dx)+(dy*dy));
			
			if(d>255)
				res[width*row+col]=0;
			else
				res[width*row+col]=255;
		}
		res[width*row+col]=data[width*row+col];								//writing last row to result
	}
	return res;
}

int main(int argc,char* argv[])
{
	if(argc==2)
		printf("\nThe file name is:%s",argv[1]);
	else if(argc>2)
	{
		printf("\nToo many arguments");
		return 0;
	}
	else
	{
		printf("\nOne argument expected");
		return 0;
	}
	if(!filecheck_bmp(argv[1]))
	{
		printf("\nInvalid file name");
		return 0;
	}

	bmpheader bmp;
	dibheader dib;
	int size=0,check=0,height=0,width=0;
	unsigned char *bitmapData=NULL,*gray=NULL,*res=NULL;
	bitmapData = load_bmp(argv[1],&dib,&bmp);
	if(!bitmapData)
	{
		return 0;
	}
	height=dib.height;
	if(height<0)
		height=-height;
	width=dib.width;
	dib.bmp_bytes=height*width;

    	gray =(unsigned char*)malloc(dib.bmp_bytes);
	if(!gray)
	{
		printf("\nNo memory allocated");
		return 0;
	}

	gray=rgbToGray(bitmapData,gray,dib);

	res =(unsigned char*)malloc(dib.bmp_bytes);
	if(!res)
	{
		printf("\nNo memory allocated");
		return 0;
	}
	
	res=sobel(gray,res,dib);
	
	check=file_write(bmp,dib,res);
	if(!check)
	{
		printf("\nFile not written properly");
	}
	if(bitmapData)
	{
		free(bitmapData);
		bitmapData=NULL;
	}
	if(!gray)
	{
		free(gray);
		gray=NULL;
	}
	printf("\nSuccessfully converted to gray scale image and edge has been detected");
}