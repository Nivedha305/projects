#include<stdio.h>
#include<stdlib.h>

enum choice{ADD=1,FIND,SORT,DELETE,PRINT,EXIT};
#define MIN 1
#define MAX 2
#define CGPA_MIN 0
#define CGPA_MAX 10
#define PHN_MIN 1111111111
#define PHN_MAX 9999999999
#define FLAG 0
#define FIND_BY_ID 1
#define MAX_LENGTH 1000

 /************************************************************************************************************
  *  Name	:	clbuf
  *  Arguments  :       No arguments
  *  Returns	:	Does not return anything
  *  Description	:   This function is clear buffer and is used to clear input buffer in case of 
                            input buffering.
  ************************************************************************************************************/

void clbuf() 
{
    while ( getchar()!= '\n');
}

 /************************************************************************************************************
  *  Name	:	getlongint
  *  Arguments  :       input as pointer,minimum input value,maximum input value
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to get input function and to validate them.
  ************************************************************************************************************/

void getlongint(long long int *ref,long long int min,long long int max)
{
	while(!(scanf("%lld",ref)) || (*ref<min || *ref>max))
	{
		clbuf();
		printf("Invalid input\nEnter a valid number: ");
	}
	clbuf();
}

 /************************************************************************************************************
  *  Name	:	getfloat
  *  Arguments  :       input as pointer,minimum value,maximum value
  *  Returns	:	Does not return anything
  *  Description	:   This function is get float input function and to validate them.
  ************************************************************************************************************/

void getfloat(float *ref,float min,float max)
{
	while(!(scanf("%f",ref))|| (*ref<min || *ref>max))
	{
		clbuf();
		printf("\nInvalid input\nEnter a valid CGPA: ");
	}
}

 /************************************************************************************************************
  *  Name	:	getinput
  *  Arguments  :       No arguments
  *  Returns	:	Returns character pointer
  *  Description	:   This function is used to get char input dynamically.
  ************************************************************************************************************/

char* getinput()
{
	int len=0;
	char *cptr=NULL;
	char ch;
        cptr=(char*) malloc(sizeof(char));
	if(cptr==NULL)
        {
		printf("No memory allocated");
		return NULL;
	}
        while(scanf("%c",&ch) && ch != '\n')
	{
		cptr[len]=ch;   
		len++; 
		cptr=(char*) realloc(cptr,sizeof(char)*len+1);                                               
	} 
	cptr[len]='\0';
	return cptr; 
}

/************************************************************************************************************
  *  Name	:	str_cmp
  *  Arguments  :       string1 and string2
  *  Returns	:	Returns the difference of ascii value between the two strings if they are not same
                        else returns zero.
  *  Description	:   This function is used to compare two strings.
  ************************************************************************************************************/

int str_cmp(char* string1,char* string2)
{
    int index = 0;
    while (string1[index] == string2[index]) 
    {									 
	if(string1[index] == '\0' && string2[index] == '\0')				
	{
 	    break;
	}
 	index++;
   }
   return ((string1[index]-'0')-(string2[index]-'0'));
}

/************************************************************************************************************
                            Declaration of structure for student database
  ************************************************************************************************************/

struct student{
	char *name;
	char *clg_name;
	char *clg_place;
        float cgpa;
        long long int phn_num,id;
};

 /************************************************************************************************************
  *  Name	:	print details
  *  Arguments  :       structure pointer
  *  Returns	:	Does not return anything.
  *  Description	:   Used to print the student database.
  ************************************************************************************************************/

void print_details(struct student* ptr)
{
	if(ptr->id)
	{
                
		printf("\nStudent id:%lld",(ptr->id));
		printf("\nStudent name:%s",(ptr->name));
		printf("\nCollege name:%s",(ptr->clg_name));
		printf("\nCollege place:%s",(ptr->clg_place));
		printf("\nCGPA:%f",(ptr->cgpa));
		printf("\nPhone number:%lld\n",(ptr->phn_num));
	}
}
 /************************************************************************************************************
  *  Name	:	find by id
  *  Arguments  :       structure pointer, length
  *  Returns	:	Returns a flag which indicates the indexes of the found id.
  *  Description	:   Used to find the id.
  ************************************************************************************************************/
int find_by_id(struct student* ptr,int len)
{
	long long int find_id=0;
        int index,flag=0;
	printf("\nEnter student id:");
	getlongint(&find_id,MIN,INT_MAX);
	
	for(index=0;index<len;index++)
	{
		if((ptr+index)->id == find_id)
		{
			flag=flag|(1<<index); 
		}
	}
	return flag;
}

/************************************************************************************************************
  *  Name	:	find by name
  *  Arguments  :       structure pointer, length
  *  Returns	:	Returns a flag which indicates the indexes of the found id.
  *  Description	:   Used to find the id.
  ************************************************************************************************************/
int find_by_name(struct student* ptr, int len)
{
	char* find_name;
	printf("\nEnter the name:");
	find_name=getinput();
	int index,flag=0;
	for(index=0;index<len;index++)
	{
		if(str_cmp((ptr+index)->name,find_name)==0)
		{
			flag=(flag|(1<<index));      
		}
	}
	return flag;
}
/************************************************************************************************************
  *  Name	:	sort by id
  *  Arguments  :       structure pointer, length
  *  Returns	:	Does not return anything
  *  Description	:   Used to sort the structure by id.
  ************************************************************************************************************/
void sort_by_id(struct student *ptr, int len)
{
	int index1,index2;
	struct student temp;
	
	for(index1=0;index1<len;index1++)
	{
		for(index2=index1;index2<len;index2++)
		{
			if((ptr+index1)->id > (ptr+index2)->id)
			{
			temp =*(ptr+index1);
		        *(ptr+index1) = *(ptr+index2);
			*(ptr+index2) = temp;
			}
		}
	}
	
	for(index1=0;index1<len;index1++)
		print_details(ptr+index1);	
}
/************************************************************************************************************
  *  Name	:	sort by name
  *  Arguments  :       structure pointer, length
  *  Returns	:	Does not return anything
  *  Description	:   Used to sort the structure by name.
  ************************************************************************************************************/
void sort_by_name(struct student *ptr, int len)
{
	int index1,index2;
	struct student temp;
	
	for(index1=0;index1<len;index1++)
	{
		for(index2=index1;index2<len;index2++)
		{
			if(str_cmp((ptr+index1)->name,(ptr+index2)->name)>0)
			{
			temp =*(ptr+index1);
		        *(ptr+index1) = *(ptr+index2);
			*(ptr+index2) = temp;
			}
		}
	}
	
	for(index1=0;index1<len;index1++)
		print_details(ptr+index1);	
}


 /************************************************************************************************************
  *  Name	:	delete
  *  Arguments  :       structure pointer, length
  *  Returns	:	Does not return anything
  *  Description	:   Used to delete the structure element.
  ************************************************************************************************************/

void delete(struct student *ptr,int *len,int value)
{
	int count=0,index;		
	while(value>0)
	{	
		if(value%2)                       //checking for odd number to find index as we shift value to right
		{
			struct student *sptr=(ptr+count);
			sptr->id=0;
			sptr->name='\0';
			sptr->clg_name='\0';
			sptr->clg_place='\0';
			sptr->cgpa=0;
			sptr->phn_num=0;
			for(index=count;index<*len;index++)
				*(ptr+index)=*(ptr+index+1);
			*len=*len-1;
		}
		count++;
		value=value>>1;
	}
	printf("\nEntry successfully deleted");
}
/************************************************************************************************************
  *  Name	:	add details
  *  Arguments  :       structure pointer
  *  Returns	:	Does not return anything
  *  Description	:  Used to add input to student database.
  ************************************************************************************************************/

void add_details(struct student* ptr,int position,int len)
{
	int index;
	struct student *sptr;
	for(index=len;index>position;index--)
	{
		sptr=(ptr+index);

		sptr->id=(sptr-1)->id;
		sptr->name=(sptr-1)->name;
		sptr->clg_name=(sptr-1)->clg_name;
		sptr->clg_place=(sptr-1)->clg_place;
		sptr->cgpa=(sptr-1)->cgpa;
		sptr->phn_num=(sptr-1)->phn_num;
	}
	sptr=(ptr+position);
	printf("\nEnter the student id:");
	getlongint(&(sptr->id),MIN,INT_MAX);
	printf("\nEnter the student name:");                    
	sptr->name=getinput();
	printf("\nEnter the college name:");
	sptr->clg_name=getinput();
	printf("\nEnter the college place:");
	sptr->clg_place=getinput();
	printf("\nEnter the CGPA:");
	getfloat(&(sptr->cgpa),CGPA_MIN,CGPA_MAX);
	printf("\nEnter the phone number:");
	getlongint(&(sptr->phn_num),PHN_MIN,PHN_MAX);
}

/************************************************************************************************************
  *  Name	:	str_len
  *  Arguments  :       string1 
  *  Returns	:	Returns integer value
  *  Description	:   This function is used to find the length of the string
  ************************************************************************************************************/

int str_len(char *str)
{
	if(str==NULL)
		return 0;
    	int index;
    	for(index=0;str[index]!='\0';index++);
    	return index;
}

/************************************************************************************************************
  *  Name	:	str_cpy
  *  Arguments  :       string1 and string2
  *  Returns	:	Returns the char pointer of the destination
  *  Description	:   This function is used to copy one string to another.
  ************************************************************************************************************/

char *str_cpy(char *dest, char *source)
{
	char *temp=dest;
	while(*source!='\0')
       		*dest++ = *source++;
    	*dest = '\0';
    	return temp;
}

/************************************************************************************************************
  *  Name	:	str_str
  *  Arguments  :       char pointers string1 and string2
  *  Returns	:	Returns the char pointer
  *  Description	:   This function is used to find the occurrence of string2 in string1
  ************************************************************************************************************/

char* str_str(char *s1, char *s2)
{
        int index1=0,index2=0;
 	while (s1[index1]!='\0')     
	{                
		while (s2[index2]!='\0' && s1[index2+index1]==s2[index2])    
		{                                                                         
			index2++;						
		}
		if(s2[index2]=='\0')    
			return s1+index1;
		index1++; 
		index2=0;
	}
	return 0;
}	

/************************************************************************************************************
  *  Name	:	find_xml
  *  Arguments  :       char pointers xml input, front tag, back tag
  *  Returns	:	Returns the char pointer of tag value
  *  Description	:   This function is used to find the xml tag value
  ************************************************************************************************************/			

char* find_xml(char *temp1,char* front,char* back)
{ 
        char *word=NULL,*temp2=NULL;
	int index=0;
        temp1=str_str(temp1,front);
	temp1=temp1+str_len(front);
	temp2=temp1;
	temp2=str_str(temp2,back);
	word=(char*)malloc(((temp2-temp1)+1)*sizeof(char));
	for(index=0;index<temp2-temp1;index++)
		*(word+index)=*(temp1+index);
	*(word+index)='\0';
        return word;
}

/************************************************************************************************************
  *  Name	:	check
  *  Arguments  :       char pointers input,front tag
  *  Returns	:	Returns integer
  *  Description	:   This function is used to check if all the details are present
  ************************************************************************************************************/

int check(char *input,char *tag)
{
	int count=0;
	char *temp1=input;
	temp1=str_str(temp1,tag);
	while(temp1 !=NULL)
	{
		temp1=temp1+str_len(tag);
		temp1=str_str(temp1,tag);
		count++;
	}
	return count;
}		

/************************************************************************************************************
  *  Name	:	add_from_xml
  *  Arguments  :       char pointers xml input,struct pointer, length
  *  Returns	:	Returns the struct pointer
  *  Description	:   This function is used to add the xml data to existing structure
  ************************************************************************************************************/

struct student* add_from_xml(char *input,struct student *ptr,int* len)
{
	char *temp1=NULL,*strtol_ptr;
	int count=0,index,index1,number;
	count=check(input,"<student");
	if(count==0)
		return NULL;
	else
		number=count;
	count=check(input,"<id");
	if(count!=number)
	{
		printf("Id not found");
		return NULL;
	}
	count=check(input,"<name");
	if(count!=number)
	{
		printf("Name not found");
		return NULL;
	}
	count=check(input,"<clg_name");
	if(count!=number)
	{
		printf("College name not found");
		return NULL;
	}
	count=check(input,"<clg_place");
	if(count!=number)
	{
		printf("College place not found");
		return NULL;
	}
	count=check(input,"<cgpa");
	if(count!=number)
	{
		printf("CGPA not found");
		return NULL;
	}
	count=check(input,"<phn_num");
	if(count!=number)
	{
		printf("Phone number not found");
		return NULL;
	}
	*len=count;
	ptr=(struct student*) realloc(ptr,((count)*sizeof(struct student)));
	if(ptr==NULL)
	{
		printf("No memory allocated");
		return 0;
	}
	temp1=input;
	for(index=0;index<count;index++)
	{
		struct student *sptr=(ptr+index);
		temp1=str_str(temp1,"<id>");
		temp1=temp1+str_len("<id>");
		sptr->id=atoi(temp1);

		sptr->name=find_xml(temp1,"<name>","</name>");

		sptr->clg_name=find_xml(temp1,"<clg_name>","</clg_name>");
	
		sptr->clg_place=find_xml(temp1,"<clg_place>","</clg_place>");
	
		temp1=str_str(temp1,"<cgpa>");
		temp1=temp1+str_len("<cgpa>");
		sptr->cgpa=atof(temp1);
	
		temp1=str_str(temp1,"<phn_num>");
		temp1=temp1+str_len("<phn_num>");
		sptr->phn_num=strtoll(temp1,&strtol_ptr,10);
	}
	return ptr;
}


int main()
{
	int len=0,value=0,count=0,index,length=MAX_LENGTH,xml_len=0;
	long long int position=1,find_menu,choice,max=0;
	char *string,*xml_buffer=NULL;
	FILE *fp;
	string=(char*)malloc(length*(sizeof(char)));
	if(string==NULL)
	{
		printf("No memory allocated");
		return 0;
	}
	struct student *ptr=NULL;
	char *buffer=NULL,*temp_buffer=NULL,*input=NULL;
	ptr=(struct student*) malloc((len+1)*sizeof(struct student));
	if(ptr==NULL)
	{
		printf("No memory allocated");
		return 0;
	}

	fp=fopen("test.xml","r+");
	if(fp==NULL)
	{
		printf("File does not open");
		return 0;
	}

	fseek(fp,0,SEEK_END);
	xml_len=ftell(fp);
	rewind(fp);

	xml_buffer=(char*) malloc((xml_len+1)*sizeof(char));
	if(xml_buffer==NULL)
	{
		printf("No memory allocated");
		return 0;
	}

	fread(xml_buffer,sizeof(char),xml_len,fp);
	fclose(fp);

	*(xml_buffer+(xml_len))='\0';
	
	ptr=add_from_xml(xml_buffer,ptr,&len);
	if(ptr==NULL)
	{
		return 0;
	}
	
	if(xml_buffer)
	{
		free(xml_buffer);
		xml_buffer=NULL;
	}
	do {	
		printf("\nMenu:\n1.Add\n2.Find\n3.Sort\n4.Delete\n5.Print\n6.Exit\n");
		printf("Enter your choice:");
		max=6;
		getlongint(&choice,MIN,max);
		switch(choice)
		{
			case ADD:
				printf("\nEnter the student details:");
				if(len)
				{       
					printf("\nEnter the position:");
					max=len+1;
					getlongint(&position,MIN,max);
					ptr=(struct student*) realloc(ptr,((len+1)*sizeof(struct student)));
					if(ptr==NULL)
					{
						printf("No memory allocated");
						return 0;
					}
				}		
				add_details(ptr,position-1,len);
				len++;
				break;

			case FIND:
				printf("\n1.By ID\n2.By NAME\n");
				printf("Enter your choice:");
				getlongint(&find_menu,MIN,MAX);
				if(find_menu==FIND_BY_ID)
					value=find_by_id(ptr,len);
				else 
					value=find_by_name(ptr,len);
				count=0;
				if(!value)
					printf("Cannot be found");
				while(value>0)
				{	
					if(value==(value&1))
					{
						print_details(ptr+count);
					}
					count++;
					value=value>>1;
				}
				break;

			case SORT:
				printf("\n1.By ID\n2.By NAME\n");
				printf("Enter your choice:");
				getlongint(&find_menu,MIN,MAX);
				if(find_menu==FIND_BY_ID)
					sort_by_id(ptr,len);
				else 
					sort_by_name(ptr,len);
				break;

			case DELETE: 
				printf("\n1.By ID\n2.By NAME\n");
				printf("Enter your choice:");
				getlongint(&find_menu,MIN,MAX);
				if(find_menu==FIND_BY_ID)
					value=find_by_id(ptr,len);
				else 
					value=find_by_name(ptr,len);
				count=0;
				if(!value)
					printf("Cannot be found");
				else
					delete(ptr,&len,value);
				break;
			case PRINT:
				for(index=0;index<len;index++)
					print_details(ptr+index);
				break;

			case EXIT:
				buffer=(char*) malloc(length*sizeof(char));
				temp_buffer=buffer;
				if(buffer==NULL)
				{
					printf("No memory allocated");
					break;
				}
				str_cpy(temp_buffer,"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<database>\n");
				temp_buffer=temp_buffer+str_len(temp_buffer);
				for(index=0;index<len;index++)
				{
					sprintf(string,"  <student%d>\n    <id>%lld</id>\n    <name>%s</name>\n    <clg_name>%s</clg_name>\n    <clg_place>%s</clg_place>\n    <cgpa>%f</cgpa>\n    <phn_num>%lld</phn_num>\n  </student%d>\n",
					index+1,(ptr+index)->id,(ptr+index)->name,(ptr+index)->clg_name,(ptr+index)->clg_place,(ptr+index)->cgpa,(ptr+index)->phn_num,index+1);
					str_cpy(temp_buffer,string);
					if(index!=len-1)
					{
						length=length+length;
						buffer=(char*)realloc(buffer,length*(sizeof(char)));
						if(buffer==NULL)
						{
							printf("No memory allocated");
							break;		
						}	
						temp_buffer=buffer;
						temp_buffer=temp_buffer+str_len(buffer);
					}
				}
				temp_buffer=temp_buffer+str_len(temp_buffer);
				str_cpy(temp_buffer,"</database>");

				fp=fopen("test.xml","w");
				if(fp==NULL)
				{
					printf("File does not open");
					return 0;
				}
				int buffer_len=str_len(buffer);
				fwrite(buffer,sizeof(char),buffer_len,fp);
				fclose(fp);
				if(buffer)
				{
					free(buffer);
					buffer=NULL;
				}  
				
				if(string)
				{
					free(string);
					string=NULL;
				}
				if(ptr)
				{
					for(index=0;index<len;index++)
					{
						if((ptr+index)->name)
						{
							free((ptr+index)->name);
							(ptr+index)->name=NULL;
						}
						if((ptr+index)->clg_name)
						{
							free((ptr+index)->clg_name);
							(ptr+index)->clg_name=NULL;
						}
						if((ptr+index)->clg_place)
						{
							free((ptr+index)->clg_place);
							(ptr+index)->clg_place=NULL;
						}
					}
					free(ptr);
					ptr=NULL;
				}
				return 0;
		}
	}while(1);
}
