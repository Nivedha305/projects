#include<stdio.h>
#include<stdlib.h>

enum choice{ADD=1,FIND,SORT,DELETE,EXIT};
#define MIN 1
#define MAX 2
#define CGPA_MIN 0
#define CGPA_MAX 10
#define PHN_MIN 1111111111
#define PHN_MAX 9999999999
#define FLAG 0
#define FIND_BY_ID 1

 /************************************************************************************************************
  *  Name	:	clbuf
  *  Arguments  :       No arguments
  *  Returns	:	Does not return anything
  *  Description	:   This function is clear buffer and is used to clear input buffer in case of 
                            input buffering.
  ************************************************************************************************************/

void clbuf() 
{
    while ( getchar()!= '\n');
}

 /************************************************************************************************************
  *  Name	:	getlongint
  *  Arguments  :       input as pointer,minimum input value,maximum input value
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to get input function and to validate them.
  ************************************************************************************************************/

void getlongint(long long int *ref,long long int min,long long int max)
{
	while(!(scanf("%lld",ref)) || (*ref<min || *ref>max))
	{
		clbuf();
		printf("Invalid input\nEnter a valid number: ");
	}
	clbuf();
}

 /************************************************************************************************************
  *  Name	:	getfloat
  *  Arguments  :       input as pointer,minimum value,maximum value
  *  Returns	:	Does not return anything
  *  Description	:   This function is get float input function and to validate them.
  ************************************************************************************************************/

void getfloat(float *ref,float min,float max)
{
	while(!(scanf("%f",ref))|| (*ref<min || *ref>max))
	{
		clbuf();
		printf("\nInvalid input\nEnter a valid CGPA: ");
	}
}

 /************************************************************************************************************
  *  Name	:	getinput
  *  Arguments  :       No arguments
  *  Returns	:	Returns character pointer
  *  Description	:   This function is used to get char input dynamically.
  ************************************************************************************************************/

char* getinput()
{
	int len=0;
	char *cptr=NULL;
	char ch;
        cptr=(char*) malloc(sizeof(char));
	if(cptr==NULL)
        {
		printf("No memory allocated");
		return NULL;
	}
        while(scanf("%c",&ch) && ch != '\n')
	{
		cptr[len]=ch;   
		len++; 
		cptr=(char*) realloc(cptr,sizeof(char)*len+1);                                               
	} 
	cptr[len]='\0';
	return cptr; 
}

/************************************************************************************************************
  *  Name	:	str_cmp
  *  Arguments  :       string1 and string2
  *  Returns	:	Returns the difference of ascii value between the two strings if they are not same
                        else returns zero.
  *  Description	:   This function is used to compare two strings.
  ************************************************************************************************************/

int str_cmp(char* string1,char* string2)
{
    int index = 0;
    while (string1[index] == string2[index]) 
    {									 
	if(string1[index] == '\0' && string2[index] == '\0')				
	{
 	    break;
	}
 	index++;
   }
   return ((string1[index]-'0')-(string2[index]-'0'));
}

/************************************************************************************************************
                            Declaration of structure for student database
  ************************************************************************************************************/

struct student{
	char *name;
	char *clg_name;
	char *clg_place;
        float cgpa;
        long long int phn_num,id;
};

 /************************************************************************************************************
  *  Name	:	print details
  *  Arguments  :       structure pointer
  *  Returns	:	Does not return anything.
  *  Description	:   Used to print the student database.
  ************************************************************************************************************/

void print_details(struct student* ptr)
{
	if(ptr->id)
	{
		printf("\nStudent id:%lld",(ptr->id));
		printf("\nStudent name:%s",(ptr->name));
		printf("\nCollege name:%s",(ptr->clg_name));
		printf("\nCollege place:%s",(ptr->clg_place));
		printf("\nCGPA:%f",(ptr->cgpa));
		printf("\nPhone number:%lld\n",(ptr->phn_num));
	}
}
 /************************************************************************************************************
  *  Name	:	find by id
  *  Arguments  :       structure pointer, length
  *  Returns	:	Returns a flag which indicates the indexes of the found id.
  *  Description	:   Used to find the id.
  ************************************************************************************************************/
int find_by_id(struct student* ptr,int len)
{
	long long int find_id=0;
        int index,flag=0;
	printf("\nEnter student id:");
	getlongint(&find_id,MIN,INT_MAX);
	
	for(index=0;index<len;index++)
	{
		if((ptr+index)->id == find_id)
		{
			flag=flag|(1<<index); 
		}
	}
	return flag;
}

/************************************************************************************************************
  *  Name	:	find by name
  *  Arguments  :       structure pointer, length
  *  Returns	:	Returns a flag which indicates the indexes of the found id.
  *  Description	:   Used to find the id.
  ************************************************************************************************************/
int find_by_name(struct student* ptr, int len)
{
	char* find_name;
	printf("\nEnter the name:");
	find_name=getinput();
	int index,flag=0;
	for(index=0;index<len;index++)
	{
		if(str_cmp((ptr+index)->name,find_name)==0)
		{
			flag=(flag|(1<<index));      
		}
	}
	return flag;
}
/************************************************************************************************************
  *  Name	:	sort by id
  *  Arguments  :       structure pointer, length
  *  Returns	:	Does not return anything
  *  Description	:   Used to sort the structure by id.
  ************************************************************************************************************/
void sort_by_id(struct student *ptr, int len)
{
	int index1,index2;
	struct student temp;
	
	for(index1=0;index1<len;index1++)
	{
		for(index2=index1;index2<len;index2++)
		{
			if((ptr+index1)->id > (ptr+index2)->id)
			{
			temp =*(ptr+index1);
		        *(ptr+index1) = *(ptr+index2);
			*(ptr+index2) = temp;
			}
		}
	}
	
	for(index1=0;index1<len;index1++)
		print_details(ptr+index1);	
}
/************************************************************************************************************
  *  Name	:	sort by name
  *  Arguments  :       structure pointer, length
  *  Returns	:	Does not return anything
  *  Description	:   Used to sort the structure by name.
  ************************************************************************************************************/
void sort_by_name(struct student *ptr, int len)
{
	int index1,index2;
	struct student temp;
	
	for(index1=0;index1<len;index1++)
	{
		for(index2=index1;index2<len;index2++)
		{
			if(str_cmp((ptr+index1)->name,(ptr+index2)->name)>0)
			{
			temp =*(ptr+index1);
		        *(ptr+index1) = *(ptr+index2);
			*(ptr+index2) = temp;
			}
		}
	}
	
	for(index1=0;index1<len;index1++)
		print_details(ptr+index1);	
}


 /************************************************************************************************************
  *  Name	:	delete
  *  Arguments  :       structure pointer, length
  *  Returns	:	Does not return anything
  *  Description	:   Used to delete the structure element.
  ************************************************************************************************************/

void delete(struct student *ptr,int *len,int value)
{
	int count=0,index;		
	while(value>0)
	{	
		if(value%2)                       //checking for odd number to find index as we shift value to right
		{
			(ptr+count)->id=0;
			(ptr+count)->name='\0';
			(ptr+count)->clg_name='\0';
			(ptr+count)->clg_place='\0';
			(ptr+count)->cgpa=0;
			(ptr+count)->phn_num=0;
			for(index=count;index<*len;index++)
				*(ptr+index)=*(ptr+index+1);
			*len=*len-1;
		}
		count++;
		value=value>>1;
	}
	printf("\nEntry successfully deleted");
}
/************************************************************************************************************
  *  Name	:	add details
  *  Arguments  :       structure pointer
  *  Returns	:	Does not return anything
  *  Description	:  Used to add input to student database.
  ************************************************************************************************************/

void add_details(struct student* ptr,int position,int len)
{
	int index;
		for(index=len;index>position;index--)
		{
			(ptr+index)->id=(ptr+index-1)->id;
			(ptr+index)->name=(ptr+index-1)->name;
			(ptr+index)->clg_name=(ptr+index-1)->clg_name;
			(ptr+index)->clg_place=(ptr+index-1)->clg_place;
			(ptr+index)->cgpa=(ptr+index-1)->cgpa;
			(ptr+index)->phn_num=(ptr+index-1)->phn_num;
		}
	printf("\nEnter the student id:");
	getlongint(&((ptr+position)->id),MIN,INT_MAX);
	printf("\nEnter the student name:");
	(ptr+position)->name=getinput();
	printf("\nEnter the college name:");
	(ptr+position)->clg_name=getinput();
	printf("\nEnter the college place:");
	(ptr+position)->clg_place=getinput();
	printf("\nEnter the CGPA:");
	getfloat(&((ptr+position)->cgpa),CGPA_MIN,CGPA_MAX);
	printf("\nEnter the phone number:");
	getlongint(&((ptr+position)->phn_num),PHN_MIN,PHN_MAX);
}

int main()
{
	int len=0,menu=1,value=0,count=0,index;
	long long int position=1,find_menu,choice,max=0;
	struct student *ptr=NULL;
	ptr=(struct student*) malloc((len+1)*sizeof(struct student));
	if(ptr==NULL)
	{
		printf("No memory allocated");
		return 0;
	}
	do {	
		printf("\nMenu:\n1.Add\n2.Find\n3.Sort\n4.Delete\n5.Exit\n");
		printf("Enter your choice:");
		max=5;
		getlongint(&choice,MIN,max);
		switch(choice)
		{
			case ADD:
				printf("\nEnter the student details:");
				if(len)
				{       
					printf("\nEnter the position:");
					max=len+1;
					getlongint(&position,MIN,max);
					ptr=(struct student*) realloc(ptr,((len+1)*sizeof(struct student)));
					if(ptr==NULL)
					{
						printf("No memory allocated");
						return 0;
					}
				}		
				add_details(ptr,position-1,len);
				len++;
				break;

			case FIND:
				printf("\n1.By ID\n2.By NAME\n");
				printf("Enter your choice:");
				getlongint(&find_menu,MIN,MAX);
				if(find_menu==FIND_BY_ID)
					value=find_by_id(ptr,len);
				else 
					value=find_by_name(ptr,len);
				count=0;
				if(!value)
					printf("Cannot be found");
				while(value>0)
				{	
					if(value==(value&1))
					{
						print_details(ptr+count);
					}
					count++;
					value=value>>1;
				}
				break;

			case SORT:
				printf("\n1.By ID\n2.By NAME\n");
				printf("Enter your choice:");
				getlongint(&find_menu,MIN,MAX);
				if(find_menu==FIND_BY_ID)
					sort_by_id(ptr,len);
				else 
					sort_by_name(ptr,len);
				break;

			case DELETE: 
				printf("\n1.By ID\n2.By NAME\n");
				printf("Enter your choice:");
				getlongint(&find_menu,MIN,MAX);
				if(find_menu==FIND_BY_ID)
					value=find_by_id(ptr,len);
				else 
					value=find_by_name(ptr,len);
				count=0;
				if(!value)
					printf("Cannot be found");
				else
					delete(ptr,&len,value);
				break;

			case EXIT:
				if(ptr)
				{
					for(index=0;index<len-1;index++)
					{
						if((ptr+index)->name)
						{
							free(ptr->name);
							ptr->name=NULL;
						}
						if((ptr+index)->clg_name)
						{
							free(ptr->clg_name);
							ptr->clg_name=NULL;
						}
						if((ptr+index)->clg_place)
						{
							free(ptr->clg_place);
							ptr->clg_name=NULL;
						}
					}
					free(ptr);
					ptr=NULL;
				}
				return 0;
		}
	}while(1);
}
