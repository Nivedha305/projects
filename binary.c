#include<stdio.h>

void clbuf() 
{
    while (getchar()!='\n');
};

void getint(long int *ref)
{
	printf("\nEnter the number: ");
	while(!(scanf("%ld",ref)))
	{
		clbuf();
		printf("Invalid input\nEnter the number: ");
	}
}

void print_binary(long int number)
{
    if(number<=0)
    {
        return;
    }
    print_binary(number/2);
    printf("%ld",number%2);
}
int main()
{  
    int count=0;    
    long int number;
    getint(&number);
    printf("The binary value is:");
    number?print_binary(number):printf("0");  

    while(number)
    {                                                               
   	number=(number&(number<<1));                                      
   	count++;                                                    
    }

    printf("\nThe maximum consecutive ones count is:%d",count);
    return 0;  
}  