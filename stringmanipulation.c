#include<stdio.h>
enum manipulation{STRLEN=1, STRCAT,STRCPY,STRCMP,EXIT};

void clbuf() 
{
    while (getchar()!='\n');
};

void getint(int *ref,int min,int max)
{
	printf("Enter the number: ");
	while(!(scanf("%d",ref)) || (*ref<min || *ref>max))
	{
		clbuf();
		printf("Invalid input\nEnter the number: ");
	}
}

int str_len(char string1[])
{
    int index;
    for(index=0;string1[index]!='\0';index++);
    return index;
}

int check_alphabets(char string1[])
{
    int index;
    for (index = 0; index < str_len(string1) ; index++)
    {
        if (!((string1[index] >= 'a' && string1[index] <= 'z') || (string1[index] >= 'A' && string1[index] <= 'Z') || (string1[index] == ' ')))
        {
	return 0; 
	}
    }
    return 1;
}

void str_cat(char string1[],char string2[])
{
    int index,len=str_len(string1);
    for(index=0;string2[index]!='\0';index++)
    {
        string1[len++]=string2[index];
    }
    string1[len]='\0';
}
void str_cpy(char string1[],char string2[], int index)
{
    int temp,temp1=0;
    for(temp=index-1;string2[temp1]!='\0';temp1++,temp++)
    {
    	string1[temp]=string2[temp1];
    } 
    string1[temp]='\0'; 
}

int str_cmp(char string1[],char string2[])
{
    int index = 0;
    while (string1[index] == string2[index]) 
    {									 
	if(string1[index] == '\0' && string2[index] == '\0')				
	{
	    break;
	}
 	index++;
   }
   return ((string1[index]-'0')-(string2[index]-'0'));
}

void getstring(char string1[])
{
    do
    {
  	printf("Enter the string: ");
 	scanf(" %[^\n]s",string1);
	if(!check_alphabets(string1))
		printf("Invalid input\n");
	else
		break;
    }while(1);
}

int main()
{
    int number,min=1,max=5,len,temp,index;
    char string1[100],string2[100];
    do
    {
	printf("\nMENU: \n1.Find the Length \n2.Concatenate two strings \n3.Copy from one string to another \n4.Compare two strings \n5.Exit");              
	getint(&number,min,max);
	switch(number)
	{
    		case STRLEN:
            		{
				getstring(string1);
				len=str_len(string1);
				printf("\nThe length of the string is:%d",len);	    
				break;
            		}
   		case STRCAT:
            		{
        			getstring(string1);
    				getstring(string2);
            	  		str_cat(string1,string2);
            			printf("The concatenated string is: %s",string1);
            			break;
            		}
   		case STRCPY:
           		{
        			getstring(string1);
    	   			getstring(string2);
				len=str_len(string1);
				min=0;
           			getint(&index,min,len-1);
            			str_cpy(string1,string2,index);
            			printf("The string after copying is %s",string1);
            			break;
            		}
    		case STRCMP:
    	    		{
    				getstring(string1);
    	   			getstring(string2);
    				temp=str_cmp(string1,string2);
				temp==0?printf("Both are equal"):printf("\nThe asci value diff is: %d",temp);
    				break;
	    		}
		case EXIT:
				return 0;
        	default:
        		break;       
	}	
    }while(1);
}