#include<stdio.h>
#include<stdlib.h>

enum choice{INSERT=1,DELETE,FIND,EXIT};
#define MIN 1
#define MAX 4

/************************************************************************************************************
  *  Name	:	clbuf
  *  Arguments  :       No arguments
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to clear input buffer in case of 
                            input buffering.
  ************************************************************************************************************/

void clbuf()
{
	while(getchar()!='\n');
}

/************************************************************************************************************
  *  Name	:	getint
  *  Arguments  :       input as pointer,minimum input value,maximum input value
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to get integer input and to validate them.
  ************************************************************************************************************/

void getint(int *ref,int min,int max)
{
	while(!(scanf("%d",ref)) || (*ref<min || *ref>max))
	{
	        clbuf();
		printf("Invalid input\nEnter a valid num: ");
	} 
	clbuf();
}

/************************************************************************************************************
  *  Name	:	getinput
  *  Arguments  :       No arguments
  *  Returns	:	Returns character pointer
  *  Description	:   This function is used to get char input dynamically.
  ************************************************************************************************************/

char* getinput()
{
	int len=0;
	char *cptr=NULL;
	char ch;
        cptr=(char*) malloc(sizeof(char));
	if(cptr==NULL)
        {
		printf("No memory allocated");
		return 0;
	}
        while(scanf("%c",&ch) && ch!='\n')
	{
		cptr[len]=ch;   
		len++; 
		cptr=(char*) realloc(cptr,sizeof(char)*len+1);          
	} 
	cptr[len]='\0';
	return cptr; 
}

/************************************************************************************************************
  *  Name	:	str_cmp
  *  Arguments  :       string1 and string2
  *  Returns	:	Returns the difference of ascii value between the two strings if they are not same
                        else returns zero.
  *  Description	:   This function is used to compare two strings.
  ************************************************************************************************************/

int str_cmp(char* string1,char* string2)
{
    	int index = 0;
    	while (string1[index] == string2[index]) 
    	{									 
		if(string1[index] == '\0' && string2[index] == '\0')				
		{
 	    		break;
		}
 		index++;	
	}
  	 return ((string1[index]-'0')-(string2[index]-'0'));
}

/************************************************************************************************************
                            Declaration of structure for AdjListNode
  ************************************************************************************************************/

struct AdjListNode 
{ 
   	int dest; 
    	int weight;
    	struct AdjListNode* next; 
};

/************************************************************************************************************
                            Declaration of structure for AdjList
  ************************************************************************************************************/

struct AdjList 
{ 
    	struct AdjListNode *head;  // pointer to head node of list
};

/************************************************************************************************************
                            Declaration of structure for Graph
  ************************************************************************************************************/

struct Graph 
{ 
	int num; 
    	struct AdjList* array; 
};

/************************************************************************************************************
  *  Name	:	newAdjListNode
  *  Arguments  :       integer destination, integer weight
  *  Returns	:	returns struct pointer
  *  Description	:   This function is used to create a new adjacent list node 
			    and to save the dest and weight value in the node.
  ************************************************************************************************************/

struct AdjListNode* newAdjListNode(int dest,int weight) 
{ 
	struct AdjListNode* newNode = (struct AdjListNode*) malloc(sizeof(struct AdjListNode)); 
    	newNode->dest = dest; 
   	newNode->weight=weight;
   	newNode->next = NULL; 
    	return newNode; 
} 

/************************************************************************************************************
  *  Name	:	addEdge
  *  Arguments  :       struct pointer, integer source, integer destination,integer weight.
  *  Returns	:	does not return anything
  *  Description	:   This function is used to addEdge between two nodes.
  ************************************************************************************************************/


void addEdge(struct Graph* graph, int src, int dest,int weight) 
{ 
    	struct AdjListNode* newNode = newAdjListNode(dest,weight); 
   	newNode->next = graph->array[src].head; //Node is added at the begining.
    	graph->array[src].head = newNode; 
}

/************************************************************************************************************
  *  Name	:	printGraph
  *  Arguments  :       structure pointer
  *  Returns	:	does not return anything
  *  Description	:   This function is used to print the graph.
  ************************************************************************************************************/

void printGraph(struct Graph* graph) 
{ 
    	int index; 
    	for (index = 0; index < graph->num; ++index) 
    	{ 
        	struct AdjListNode* node = graph->array[index].head; 
		printf("\n");
        	while (node) 
        	{ 
            		printf("%d -> %d (%d)",index, node->dest,node->weight); 
            		node = node->next; 
        	} 
       	} 
}
/************************************************************************************************************
  *  Name	:	free_node
  *  Arguments  :       struct pointer
  *  Returns	:	does not return anything
  *  Description	:   This function is used to free the memory allocated.
  ************************************************************************************************************/

void free_node(struct AdjListNode *node)
{
	if(node!=NULL)
	{
		if(node->next)
		{
			free_node(node->next);
			node->next=NULL;
		}
		free(node);	
		node=NULL;
	}
}

/************************************************************************************************************
  *  Name	:	delete_vertex
  *  Arguments  :       struct pointer,integer delete
  *  Returns	:	does not return anything
  *  Description	:   This function is used to delete a particular vertex from graph.
  ************************************************************************************************************/

void delete_vertex(struct Graph* graph,int delete)
{	
	for (int index = 0; index < graph->num; ++index) 
    	{
		printf("\nindex:%d",index);
		if(index!=delete)
		{
        		struct AdjListNode* node = graph->array[index].head;
			struct AdjListNode *temp=node,*prev;
	        	if(temp!=NULL && temp->dest == delete)   //deletion at the start
			{
				graph->array[index].head=temp->next;
				free(temp);
				continue;
			}
			while(temp!=NULL && temp->dest != delete)  //deletion at middle and end
			{
				prev=temp;
				temp=temp->next;
			}
			if(temp==NULL)
			{
				continue;
			}	
			prev->next=temp->next;
			free(temp);
		}
       	} 
	printGraph(graph);
	struct AdjListNode* node = graph->array[delete].head;
	free_node(node);
	graph->array[delete].head=NULL;
	printGraph(graph);
}

/************************************************************************************************************
                            Declaration of structure for MinHeapNode
  ************************************************************************************************************/

struct MinHeapNode 
{ 
    	int number; 
    	int dist; 
};

/************************************************************************************************************
                            Declaration of structure for MinHeap
  ************************************************************************************************************/

struct MinHeap 
{ 
	int size;       
    	int capacity;  
    	int *pos;     
    	struct MinHeapNode **array; 
};

/************************************************************************************************************
  *  Name	:	newMinHeapNode
  *  Arguments  :       integer number, integer distance
  *  Returns	:	returns struct pointer
  *  Description	:   This function is used to set the number and distance in to the minHeapNode.
  ************************************************************************************************************/

struct MinHeapNode* newMinHeapNode(int number, int dist) 
{ 
    	struct MinHeapNode* minHeapNode = (struct MinHeapNode*) malloc(sizeof(struct MinHeapNode)); 
    	minHeapNode->number = number; 
    	minHeapNode->dist = dist; 
    	return minHeapNode; 
}
/************************************************************************************************************
  *  Name	:	createMinHeap
  *  Arguments  :       integer capacity
  *  Returns	:	returns struct pointer
  *  Description	:   This function is used to create a minheap with the given capacity.
  ************************************************************************************************************/
struct MinHeap* createMinHeap(int capacity) 
{ 
    	struct MinHeap* minHeap = (struct MinHeap*) malloc(sizeof(struct MinHeap)); 
    	minHeap->pos = (int *)malloc(capacity * sizeof(int)); 
    	minHeap->size = 0; 
    	minHeap->capacity = capacity; 
    	minHeap->array = (struct MinHeapNode**) malloc(capacity * sizeof(struct MinHeapNode*)); 
    	return minHeap; 
} 
/************************************************************************************************************
  *  Name	:	swapMinHeapNode
  *  Arguments  :       struct double pointer, struct double pointer
  *  Returns	:	returns nothing.
  *  Description	:   This function is used to swap the values of the minHeapNode.
  ************************************************************************************************************/

void swapMinHeapNode(struct MinHeapNode** a, struct MinHeapNode** b) 
{ 
    	struct MinHeapNode* t = *a; 
    	*a = *b; 
    	*b = t; 
}

/************************************************************************************************************
  *  Name	:	minHeapify
  *  Arguments  :       struct pointer, integer idx
  *  Returns	:	returns nothing.
  *  Description	:   This function is used to heapify the minHeap.
  ************************************************************************************************************/
void minHeapify(struct MinHeap* minHeap, int idx) 
{ 
    	int smallest, left, right; 
    	smallest = idx; 
    	left = 2 * idx + 1; //index of left node
    	right = 2 * idx + 2; //index of right node
  
    	if (left < minHeap->size && 
        	minHeap->array[left]->dist < minHeap->array[smallest]->dist ) 
      	smallest = left; 
  
    	if (right < minHeap->size && 
        	minHeap->array[right]->dist < minHeap->array[smallest]->dist ) 
      	smallest = right; 
  
    	if (smallest != idx) 
    	{ 
        	struct MinHeapNode *smallestNode = minHeap->array[smallest]; 
        	struct MinHeapNode *idxNode = minHeap->array[idx]; 
  			
		// Swap positions 
  	      	minHeap->pos[smallestNode->number] = idx; 
        	minHeap->pos[idxNode->number] = smallest; 
  
        	swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]); 
  
        	minHeapify(minHeap, smallest); 
    	} 
}
/************************************************************************************************************
  *  Name	:	isEmpty
  *  Arguments  :       struct pointer.
  *  Returns	:	returns Integer.
  *  Description	:   This function is used to find whether the heap is empty.
  ************************************************************************************************************/
int isEmpty(struct MinHeap* minHeap) 
{ 
	if(minHeap->size == 0)
		return 1;
	else
		return 0; 
}

/************************************************************************************************************
  *  Name	:	extractMin
  *  Arguments  :       struct pointer.
  *  Returns	:	returns struct pointer.
  *  Description	:   This function is used to find the minimum value in the heap.
  ************************************************************************************************************/

struct MinHeapNode* extractMin(struct MinHeap* minHeap) 
{ 
    	if (isEmpty(minHeap)) 
        	return NULL; 
  
    	struct MinHeapNode* root = minHeap->array[0]; 
  	printf("\narray[0]:%u",minHeap->array[0]);
    	struct MinHeapNode* lastNode = minHeap->array[minHeap->size - 1]; 
    	minHeap->array[0] = lastNode; //replace rootnode with lastnode
	printf("\nlast:%u minHeap->array[minHeap->size - 1]:%u array[0]:%u",lastNode,minHeap->array[minHeap->size - 1],minHeap->array[0]);
  
    	minHeap->pos[root->number] = minHeap->size-1; 
    	minHeap->pos[lastNode->number] = 0; //update position of lastnode
  
    	--minHeap->size; //reduce heapsize
    	minHeapify(minHeap, 0); //heapify root
  
    	return root; 
}
/************************************************************************************************************
  *  Name	:	decreaseKey
  *  Arguments  :       struct pointer, integer number, integer distance.
  *  Returns	:	returns nothing.
  *  Description	:   This function is used to decrease the key value. 
  ************************************************************************************************************/

void decreaseKey(struct MinHeap* minHeap, int number, int dist) 
{ 
    	int index = minHeap->pos[number];	//get the position
  
    	minHeap->array[index]->dist = dist; 	//update distance

    	while (index && minHeap->array[index]->dist < minHeap->array[(index - 1) / 2]->dist) 
    	{ 
		//swap node with its parent
        	minHeap->pos[minHeap->array[index]->number] = (index-1)/2; 
        	minHeap->pos[minHeap->array[(index-1)/2]->number] = index; 
        	swapMinHeapNode(&minHeap->array[index],  &minHeap->array[(index - 1) / 2]); 
        	index = (index - 1) / 2;  	//move to paretn index
    	} 
} 

/************************************************************************************************************
  *  Name	:	isInMinHeap
  *  Arguments  :       struct pointer, integer number.
  *  Returns	:	returns a integer.
  *  Description	:   This function is used to find whether the given number is present in MinHeap.
  ************************************************************************************************************/

int isInMinHeap(struct MinHeap *minHeap, int number) 
{ 
   	if (minHeap->pos[number] < minHeap->size) 
   		return 1; 
   	return 0; 
}

/************************************************************************************************************
  *  Name	:	printArr
  *  Arguments  :       integer Array distance, integer number, integer destination.
  *  Returns	:	returns nothing.
  *  Description	:   This function is used to print the shortest distance.
  ************************************************************************************************************/

void printArr(int dist[], int num,int dest) 
{ 
    	printf("Shortest distance from Source to destination is "); 
    	for (int index = 0; index < num; ++index)
	{
		if(index==dest) 
        	printf("%d", dist[index]);
	}
}

/************************************************************************************************************
  *  Name	:	destroyHeap
  *  Arguments  :       struct pointer,integer num
  *  Returns	:	returns nothing.
  *  Description	:   This function is used to free memory allocated for the minHeap.
  ************************************************************************************************************/

void destroyHeap(struct MinHeap* node,int num)
{
    	if(node)
    	{
		if(node->array)
		{
        		for(int index=0;index<num;index++)
			{
				struct MinHeapNode* temp = node->array[index];	
				if(temp)
				{
					free(temp);
					temp=NULL;
				}
			}	
            		free(node->array);
			node->array=NULL;
        	}
		if(node->pos)
		{
			free(node->pos);
			node->pos=NULL;
		}
        	free(node);
		node=NULL;
    	}
}

/************************************************************************************************************
  *  Name	:	dijkstra
  *  Arguments  :       struct pointer, integer source, integer destination.
  *  Returns	:	returns nothing.
  *  Description	:   This function is used to implement the dijkstra algorithm.
  ************************************************************************************************************/


void dijkstra(struct Graph* graph, int src,int dest) 
{ 
    	int num = graph->num; 
    	int dist[num];    
  
    	struct MinHeap* minHeap = createMinHeap(num);	 //create minheap with number of vertices
	struct MinHeap* temp=minHeap;
  	
	//initialise minheap with distance values as INT_MAX
    	for (int index = 0; index < num; ++index) 
    	{ 
        	dist[index] = INT_MAX; 
        	minHeap->array[index] = newMinHeapNode(index, dist[index]); 
        	minHeap->pos[index] = index; 
    	} 

    	dist[src] = 0; //make distance value of source 0
    	decreaseKey(minHeap, src, dist[src]); 
  
    	minHeap->size = num; 
  
    	while (!isEmpty(minHeap)) 
    	{ 
        	struct MinHeapNode* minHeapNode = extractMin(minHeap);     //extract vertex with min value 
        	int u = minHeapNode->number;	 //store the extracted vertex number
  
        	struct AdjListNode* node = graph->array[u].head; 
		//traverse through all the adjacent nodes of u and update their distances
        	while (node != NULL) 
        	{ 
            		int v = node->dest;  
            		if (isInMinHeap(minHeap, v) && dist[u] != INT_MAX &&  node->weight + dist[u] < dist[v]) 
            		{ 
                		dist[v] = dist[u] + node->weight; 
                		decreaseKey(minHeap, v, dist[v]);      //update in minheap
            		} 
            		node= node->next; 
        	}
		if(minHeapNode)
		{
			free(minHeapNode);
			minHeapNode=NULL;
		} 
    	} 
    	printArr(dist, num,dest);
	//destroyHeap(temp,num);
}

/************************************************************************************************************
  *  Name	:	destroyGraph
  *  Arguments  :       struct pointer
  *  Returns	:	returns nothing.
  *  Description	:   This function is used to free memory allocated for the graph.
  ************************************************************************************************************/

void destroyGraph(struct Graph* graph)
{
    	if(graph)
    	{
        	if(graph->array)
        	{
            		int index;
            		for (index = 0; index < graph->num; index++)
            		{
                		struct AdjListNode* adjListPtr = graph->array[index].head;
                		while (adjListPtr)
                		{
                    			struct AdjListNode* tmp = adjListPtr;
                    			adjListPtr = adjListPtr->next;
                    			free(tmp);
                		}
            		}
            		free(graph->array);
        	}
        	free(graph);
    	}
}

int main()
{
	int choice,element=0,arr_size=0,index=0,src,des,num=0,weight,prev_arr_size=0,index1;
	char **arr;
	char *source=NULL,*dest=NULL,*delete=NULL,*from=NULL,*to=NULL;
	arr= (char**)malloc(sizeof(char*));
	if(arr==NULL)
	{
		printf("No memory allocated");
		return 0;
	}
	struct Graph* graph = (struct Graph*) malloc(sizeof(struct Graph));
	if(graph==NULL)
	{
		printf("No memory allocated");
		return 0;
	}
	graph->num = num;
	graph->array = (struct AdjList*) malloc((num+1) * sizeof(struct AdjList));
	if(graph->array==NULL)
	{
		printf("No memory allocated");
		return 0;
	}
	do{
		printf("\nMenu:\n1.Insert\n2.Delete\n3.Find shortest distance\n4.Exit\n");
		printf("Enter your choice:");
		getint(&choice,MIN,MAX);
		switch(choice)
		{
			case INSERT:
				printf("\nEnter the source:");
				source=getinput();
				printf("\nEnter the destination:");
				dest=getinput();
				printf("\nEnter the distance:");
				getint(&weight,MIN,INT_MAX);
				
				for(index=0;index<arr_size;index++)
				{
					if(str_cmp(source,*(arr+index))==0)
					{
						if(source)
						{
							free(source);
							source=NULL;
						}
						break;
					}
				}
				src=index;
				if(index==arr_size)
				{
					arr_size=arr_size+1;
					arr=(char**)realloc(arr,arr_size*sizeof(char*));
					if(arr==NULL)
					{
						printf("No memory allocated");
						break;
					}
					*(arr+src)=source;
				}
				for(index=0;index<arr_size;index++)
				{
					if(str_cmp(dest,*(arr+index))==0)
					{
						if(dest)
						{
							free(dest);
							dest=NULL;
						}
						break;
					}
				}
				des=index;
				if(index==arr_size)
				{
					arr_size=arr_size+1;
					arr=(char**)realloc(arr,arr_size*sizeof(char*));
					if(arr==NULL)
					{
						printf("No memory allocated");
						break;
					}
					*(arr+des)=dest; 
				}
				graph = (struct Graph*) realloc(graph,arr_size*sizeof(struct Graph));
				graph->num = arr_size; 
  
    				graph->array = (struct AdjList*) realloc(graph->array, arr_size*sizeof(struct AdjList));
     				if(graph->array==NULL)
				{
					printf("No memory allocated");
					break;
				}
    				for (index = prev_arr_size; index < arr_size; ++index) 
				{
        				graph->array[index].head = NULL;
				} 
				prev_arr_size=arr_size;
				addEdge(graph,src,des,weight);
				printGraph(graph);
				break;
			case DELETE:
				printf("\nEnter the city to be deleted:");
				delete=getinput();
				for(index=0;index<arr_size;index++)
				{
					if(str_cmp(delete,*(arr+index))==0)
						break;
				}
				
				if(index==arr_size)
					printf("No city found");
				else
				{
					delete_vertex(graph,index);
					*(arr+index)='\0';
				}
				if(delete)
				{
					free(delete);
					delete=NULL;
				}
				break;
			case FIND:
				printf("\nEnter the source:");
				from=getinput();
				printf("\nEnter the destination:");
				to=getinput();
				for(index=0;index<arr_size;index++)
				{
					if(str_cmp(from,*(arr+index))==0)
						break;
				}
				for(index1=0;index1<arr_size;index1++)
				{
					if(str_cmp(to,*(arr+index1))==0)
						break;
				}
				dijkstra(graph,index,index1);
				if(from)
				{
					free(from);
					from=NULL;
				}
				if(to)
				{
					free(to);
					to=NULL;
				}
				break;
			case EXIT:
				printGraph(graph);
				if(arr)
				{
					for(index=0;index<arr_size;index++)
					{
						char* temp=*(arr+index);
						if(temp)
						{
							free(temp);
							temp=NULL;
						}
					}
					free(arr);
					arr=NULL;
				}	
				
				destroyGraph(graph);
				return 0;
		}
	}while(1);
			
}




