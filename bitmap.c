#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

enum choice{ROTATE=1,FLIP,CROP,ZOOM,EXIT};
enum rotate{ROTATE_90=1,ROTATE_180,ROTATE_270,BACK};
enum flip{HORIZONTAL=1,VERTICAL,FLIP_BACK};
enum menu{FLIP_MENU_MAX=3,ROTATE_MENU_MAX,MAIN_MENU_MAX};	
#define MIN 1
#define CROP_MIN 0
#define ZOOM_MIN 2
#define ZOOM_MAX 5
#define MAGIC_ARRAY_SIZE 2

/************************************************************************************************************
  *  Name	:	clbuf
  *  Arguments  :       No arguments
  *  Returns	:	Does not return anything
  *  Description	:   This function is clear buffer and is used to clear input buffer in case of 
                            input buffering.
  ************************************************************************************************************/
void clbuf()
{
	while(getchar()!='\n');
}

/************************************************************************************************************
  *  Name	:	getint
  *  Arguments  :       int pointer,minimum value,maximum value
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to get integer input and to validate them.
  ************************************************************************************************************/

void getint(int *ref,int min,int max)
{
	while(!(scanf("%d",ref)) || (*ref<min || *ref>max))
	{
	        clbuf();
		printf("Invalid input\nEnter a valid num: ");
	} 
	clbuf();
}


/************************************************************************************************************
                            Declaration of array for 'BM' in bmp header
  ************************************************************************************************************/

unsigned char magic[2];

/************************************************************************************************************
                            Declaration of structure for bmp header
  ************************************************************************************************************/
typedef struct
{
    	uint32_t size; 
    	uint16_t reserved1;  
    	uint16_t reserved2;  
    	uint32_t offset;
  
}bmpheader;

/************************************************************************************************************
                            Declaration of structure for dib header
  ************************************************************************************************************/

typedef struct 
{
    	uint32_t header_sz; 
    	uint32_t width;  
    	uint32_t height;  
    	uint16_t nplanes; 
    	uint16_t bitcount; 
    	uint32_t compression;
    	uint32_t bmp_bytes;  
    	uint32_t hres;  
    	uint32_t vres;  
    	uint32_t ncolors;  
    	uint32_t nimpcolors; 

}dibheader;

/************************************************************************************************************
  *  Name	:	load_bmp
  *  Arguments  :       filename,dib header,bmp header
  *  Returns	:	returns the bitmapdata
  *  Description	:   This function is used to load the bmp file.
  ************************************************************************************************************/

unsigned char *load_bmp(char *filename, dibheader *dib,bmpheader *bmp)
{
    	FILE *fp;
    	unsigned char *bitmapImage=NULL; 
	int size;

    	fp = fopen(filename,"rb");
    	if (fp == NULL)
    	{
		printf("\nFile does not open");	
		fclose(fp);
        	return NULL;
    	}
    	size=fread(&magic,sizeof(char),MAGIC_ARRAY_SIZE,fp);
	if(size!=MAGIC_ARRAY_SIZE)
	{
		printf("\nFile not read correctly");
		fclose(fp);	
        	return NULL;
	}

    	size=fread(bmp,1, sizeof(bmpheader),fp);
	if(size!=sizeof(bmpheader))
	{
		printf("\nFile not read correctly");
		fclose(fp);
		return NULL;
	}

    	size=fread(dib,1, sizeof(dibheader),fp); 
	if(size!=sizeof(dibheader))
	{
		printf("\nFile not read correctly");
		fclose(fp);
		return NULL;
	}

    	fseek(fp, bmp->offset, SEEK_SET);              		      	//move the pointer to the start of image data

	bitmapImage = (unsigned char*)malloc(dib->bmp_bytes);         	//allocate memory for the size of image data
	if(!bitmapImage)
	{
		printf("\nNo memory allocated");
		fclose(fp);
		return NULL;
	}

    	fread(bitmapImage,dib->bmp_bytes,1,fp);

    	if (bitmapImage == NULL)
    	{
		printf("\nFile not read correctly");
        	fclose(fp);
        	return NULL;
    	}

    	fclose(fp);
    	return bitmapImage;
}



/************************************************************************************************************
  *  Name	:	rotate_90
  *  Arguments  :       data,output buffer,height,width
  *  Returns	:	returns the buffer
  *  Description	:   This function is used to rotate the bmp file to 90 degree
  ************************************************************************************************************/

unsigned char* rotate_90(unsigned char* data,unsigned char *buffer, int height,int width)
{
	int row,col;
	for(row=0;row<height;row++)
	{
		for(col=0;col<width;col++)
		{
			buffer[(col*height+(height-row-1))*3]=data[(width * row +col)*3];
			buffer[(col*height+(height-row-1))*3+1]=data[(width * row +col)*3+1];
			buffer[(col*height+(height-row-1))*3+2]=data[(width * row +col)*3+2];
		}	
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	flip_vertically
  *  Arguments  :       data,output buffer,height,width
  *  Returns	:	returns the buffer
  *  Description	:   This function is used to flip the bmp file horizontally
  ************************************************************************************************************/

unsigned char* flip_vertically(unsigned char* data,unsigned char *buffer, int height,int width)
{
	int row,col;
	for(row=0;row<height;row++)
	{
		for(col=0;col<width;col++)
		{
			buffer[(col+(width*(height-row-1)))*3]=data[(width * row +col)*3];
			buffer[(col+(width*(height-row-1)))*3+1]=data[(width * row +col)*3+1];
			buffer[(col+(width*(height-row-1)))*3+2]=data[(width * row +col)*3+2];
		}	
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	flip_horizontally
  *  Arguments  :       data,output buffer,height,width
  *  Returns	:	returns the buffer
  *  Description	:   This function is used to flip the bmp file horizontally
  ************************************************************************************************************/

unsigned char* flip_horizontally(unsigned char* data,unsigned char *buffer, int height, int width)
{
	int row,col;
	for(row=0;row<height;row++)
	{
		for(col=0;col<width;col++)
		{
			buffer[((width*(row+1))-col-1)*3]=data[(width * row +col)*3];
			buffer[((width*(row+1))-col-1)*3+1]=data[(width * row +col)*3+1];
			buffer[((width*(row+1))-col-1)*3+2]=data[(width * row +col)*3+2];	
		}
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	rotate_180
  *  Arguments  :       data,output buffer,height,width
  *  Returns	:	returns the buffer
  *  Description	:   This function is used to rotate the bmp file to 180 degree
  ************************************************************************************************************/

unsigned char* rotate_180(unsigned char* data,unsigned char *buffer, int height, int width)
{
	int row,col;
	for(row=0;row<height;row++)
	{
		for(col=0;col<width;col++)
		{
			buffer[((width*(height-row))-(col+1))*3]=data[(width * row +col)*3];
			buffer[((width*(height-row))-(col+1))*3+1]=data[(width * row +col)*3+1];
			buffer[((width*(height-row))-(col+1))*3+2]=data[(width * row +col)*3+2];	
		}
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	rotate_270
  *  Arguments  :       data,output buffer,height,width
  *  Returns	:	returns the buffer
  *  Description	:   This function is used to rotate the bmp file to 270 degree
  ************************************************************************************************************/

unsigned char* rotate_270(unsigned char* data,unsigned char *buffer, int height, int width)
{
	int row,col;
	for(row=0;row<height;row++)
	{
		for(col=0;col<width;col++)
		{
			buffer[((height*(width-col-1))+row)*3]=data[(width * row +col)*3];
			buffer[((height*(width-col-1))+row)*3+1]=data[(width * row +col)*3+1];
			buffer[((height*(width-col-1))+row)*3+2]=data[(width * row +col)*3+2];	
		}
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	crop
  *  Arguments  :       dib header,data,output buffer,x1,y1,x2,y2
  *  Returns	:	returns the buffer
  *  Description	:   This function is used to crop the bmp file
  ************************************************************************************************************/

unsigned char* crop(dibheader *dib,unsigned char* data,unsigned char *buffer, int x1,int y1,int x2,int y2)
{
	int height=-(dib->height),index;
	int width=dib->width,count=0,padding;
	int new_width=(y2-y1+1);
	if((new_width*3)%4)
		padding=4-((new_width*3)%4);
	
	for(int row=x1;row<=x2;row++)
	{
		for(int col=y1;col<=y2;col++,count+=3)
		{
			buffer[count]=data[(width * row +col)*3 ];
			buffer[(count+1)]=data[(width * row +col)*3+1 ];
			buffer[(count+2)]=data[(width * row +col)*3+2 ];
		}
		for(index=0;index<padding;index++,count++)
		{
			buffer[count]=0;
		}
	}
	
	return buffer;
}

/************************************************************************************************************
  *  Name	:	diff
  *  Arguments  :       integer a,integer b,integer zoom factor
  *  Returns	:	returns the difference between a and b divided by the zoom factor
  *  Description	:   This function is used to crop the bmp file
  ************************************************************************************************************/

int diff(int a,int b,int zoom)
{
	return (b-a)/zoom;
}

/************************************************************************************************************
  *  Name	:	zoom_width
  *  Arguments  :       dib header,data,output buffer,integer zoom,integer original_padding
  *  Returns	:	returns the output buffer
  *  Description	:   This function is used to zoom the bmp file row wise
  ************************************************************************************************************/

unsigned char* zoom_width(dibheader *dib,unsigned char* data,unsigned char *buffer,int zoom,int original_padding)
{
	int height=-(dib->height);
	int width=dib->width;
	int p1_b,p1_g,p1_r,p2_b,p2_g,p2_r,count=0,index=0;
	int op1,op2,op3,padding=0;
	for(int row=0,padding=0;row<height;row++,padding+=original_padding)
	{
		for(int col=0;col<width-1;col++)
		{
			p1_b=data[(width * row +col)*3 +padding];			
			p2_b=data[(width * row +(col+1))*3 +padding];
			op1=diff(p1_b,p2_b,zoom);

			p1_g=data[(width * row +col)*3+1 +padding];
			p2_g=data[(width * row +(col+1))*3+1 +padding];
			op2=diff(p1_g,p2_g,zoom);

			p1_r=data[(width * row +col)*3+2 +padding];
			p2_r=data[(width * row +(col+1))*3+2 +padding];
			op3=diff(p1_r,p2_r,zoom);

			buffer[count]=p1_b;					//copying pixel1 data to output
			buffer[count+1]=p1_g;
			buffer[count+2]=p1_r;
			count+=3;

			for(index=0;index<(zoom-1);index++,count+=3)
			{
				buffer[count]=p1_b+op1;				//adding the op values between rows
				p1_b+=op1;
				buffer[count+1]=p1_g+op2;
				p1_g+=op2;
				buffer[count+2]=p1_r+op3;
				p1_r+=op3;
			}
		}
		buffer[count]=p2_b;						//copying pixel2 data to output
		buffer[count+1]=p2_g;
		buffer[count+2]=p2_r;
		count+=3;
	}
	return buffer;
} 


/************************************************************************************************************
  *  Name	:	zoom_height
  *  Arguments  :       dib header,data,output buffer,integer zoom
  *  Returns	:	returns the output buffer
  *  Description	:   This function is used to zoom the bmp file column wise
  ************************************************************************************************************/

unsigned char* zoom_height(dibheader *dib,unsigned char *data,unsigned char *buffer,int zoom)
{	
	int height=-(dib->height);
	int width=dib->width;
	int zoom_width=(width*3);
	int p1_b,p1_g,p1_r,p2_b,p2_g,p2_r,index=0,index1=0,row,col,count=0;
	int op1,op2,op3;
	for(row=0;row<height-1;row++)
	{
		for(col=0;col<width;col++)
		{
			p1_b=data[(width * row +col)*3];
			p2_b=data[(width * (row+1) +col)*3];
			op1=diff(p1_b,p2_b,zoom);

			p1_g=data[(width * row +col)*3+1];
			p2_g=data[(width * (row+1) +col)*3+1];
			op2=diff(p1_g,p2_g,zoom);

			p1_r=data[(width * row +col)*3+2];
			p2_r=data[(width * (row+1) +col)*3+2];
			op3=diff(p1_r,p2_r,zoom);

			buffer[count]=p1_b;					//copying pixel1 data to output
			buffer[count+1]=p1_g;
			buffer[count+2]=p1_r;
			count+=zoom_width;
			
			for(index=0;index<(zoom-1);index++,count+=zoom_width)
			{
				buffer[count]=p1_b+op1;				//adding the op values between columns
				p1_b+=op1;
				buffer[count+1]=p1_g+op2;
				p1_g+=op2;
				buffer[count+2]=p1_r+op3;
				p1_r+=op3;
			}
			count=count-(zoom_width*zoom)+3;
		}
		count=count+((width*3)*(zoom-1));
	}
	for(col=0;col<width;col++,count+=3)					//copying final row data to output			
	{
		buffer[count]=data[(width * row +col)*3];
		buffer[count+1]=data[(width * row +col)*3+1];
		buffer[count+2]=data[(width * row +col)*3+2];
	}
	return buffer;
}


/************************************************************************************************************
  *  Name	:	file_write
  *  Arguments  :       bmp header,dib header,data,output buffer
  *  Returns	:	returns integer
  *  Description	:   This function is used to write the output buffer data to the file
  ************************************************************************************************************/
int file_write(bmpheader bmp,dibheader dib,unsigned char* buffer)
{
	int size;
	FILE *fp1=fopen("test.bmp","wb");
	if (fp1 == NULL)
    	{
		printf("File does not open");	
        	return 0;
    	}
	
	size=fwrite(magic,sizeof(char),MAGIC_ARRAY_SIZE,fp1);                          //writing BM data into ouput file
	if(size!=MAGIC_ARRAY_SIZE)
	{
		return 0;
	} 
	
	size=fwrite(&bmp,1,sizeof(bmpheader),fp1);			//writing bmp header data into ouput file
	if(size!=sizeof(bmpheader))
	{
		return 0;
	} 
	
	size=fwrite(&dib,1,sizeof(dibheader),fp1);			//writing dib header data into ouput file
	if(size!=sizeof(dibheader))
	{
		return 0;
	}
	
	fseek(fp1, bmp.offset, SEEK_SET);				//moving the pointer to start of image data
	
	size=fwrite(buffer,1,dib.bmp_bytes,fp1);			//writing image data into ouput file
	if(size!=dib.bmp_bytes)
	{
		return 0;
	} 

	fclose(fp1);
	if(buffer)
	{
		free(buffer);
		buffer=NULL;
	}
	return 1;
}



int main(int argc,char* argv[])
{
	if(argc==2)
		printf("\nThe file name is:%s",argv[1]);
	else if(argc>2)
	{
		printf("\nToo many arguments");
		return 0;
	}
	else
	{
		printf("\nOne argument expected");
		return 0;
	}
	bmpheader bmp;
	dibheader dib;
	unsigned char *bitmapData=NULL,*buffer=NULL,*zoom_buffer=NULL,*result_buffer=NULL;
	int height,width,choice,max,rotate_menu,flip_menu,start_height,start_width,end_height,end_width;
	int original_height=0,original_width=0,padding=0,check=1,zoom=0,original_padding=0,row=0,col=0;
	bitmapData = load_bmp(argv[1],&dib,&bmp);
	if(!bitmapData)
	{
		return 0;
	}
	do{
		printf("\nMenu:\n1.Rotate\n2.Flip\n3.Crop\n4.Zoom\n5.Exit\n");
		printf("Enter your choice:");
		getint(&choice,MIN,MAIN_MENU_MAX);
		switch(choice)
		{
			case ROTATE:
				do
				{
					printf("\nMenu:\n1.Rotate 90\n2.Rotate 180\n3.Rotate 270\n4.Back\n");
					printf("Enter your choice:");
					getint(&rotate_menu,MIN,ROTATE_MENU_MAX);
					bitmapData = load_bmp(argv[1],&dib,&bmp);
					if(!bitmapData)
					{
						return 0;
					}
					buffer=(unsigned char*)malloc(dib.bmp_bytes);
					if(buffer==NULL)
					{
						printf("No memory allocated");
						return 0;
					}
					switch(rotate_menu)
					{
						case ROTATE_90:
							height=-dib.height;
							width=dib.width;
							buffer=rotate_90(bitmapData,buffer,height,width);
							dib.height=-width;
							dib.width=height;
							check=file_write(bmp,dib,buffer);
							if(!check)
							{
								printf("File not return correctly");
								return 0;
							}
							break;
						case ROTATE_180:
							height=-dib.height;
							width=dib.width;
							buffer=rotate_180(bitmapData,buffer,height,width);
							check=file_write(bmp,dib,buffer);
							if(!check)
							{
								printf("File not return correctly");
								return 0;
							}
							break;
						case ROTATE_270:
							height=-dib.height;
							width=dib.width;
							buffer=rotate_270(bitmapData,buffer,height,width);
							dib.height=-width;
							dib.width=height;
							check=file_write(bmp,dib,buffer);
							if(!check)
							{
								printf("File not return correctly");
								return 0;
							}
							break;
						case BACK:
							if(bitmapData)
							{
								free(bitmapData);
								bitmapData=NULL;
							}
							rotate_menu=0;
							break;
					}
				}while(rotate_menu);
				break;
	
			case FLIP:
				do
				{
					printf("\nMenu:\n1.Flip horizontally\n2.Flip vertically\n3.Back\n");
					printf("Enter your choice:");
					getint(&flip_menu,MIN,FLIP_MENU_MAX);
					bitmapData = load_bmp(argv[1],&dib,&bmp);
					if(!bitmapData)
					{
						return 0;
					}
					buffer=(unsigned char*)malloc(dib.bmp_bytes);
					if(buffer==NULL)
					{
						printf("No memory allocated");
						return 0;
					}
					switch(flip_menu)
					{
						case HORIZONTAL:
							height=-dib.height;
							width=dib.width;
							buffer=flip_horizontally(bitmapData,buffer,height,width);
							check=file_write(bmp,dib,buffer);
							if(!check)
							{
								printf("\nFile not written correctly");
								return 0;
							}
			        			break;
						case VERTICAL:
							height=-dib.height;
							width=dib.width;
							buffer=flip_vertically(bitmapData,buffer,height,width);
							check=file_write(bmp,dib,buffer);
							if(!check)
							{
								printf("\nFile not written correctly");
								return 0;
							}
							break;
						case FLIP_BACK:
							if(bitmapData)
							{
								free(bitmapData);
								bitmapData=NULL;
							}
							flip_menu=0;
							break;
					}
				}while(flip_menu);
				break;	
			case CROP:
				bitmapData = load_bmp(argv[1],&dib,&bmp);
				if(!bitmapData)
				{
					return 0;
				}
				printf("\nEnter the co-ordinates:\nLeft top x1:");
				max=-(dib.height)-1;
				getint(&start_height,CROP_MIN,max);

				printf("\nLeft top y1:");
				max=dib.width-1;
				getint(&start_width,CROP_MIN,max);

				printf("\nRight bottom x2:");
				max=-(dib.height)-1;
				getint(&end_height,start_height,max);

				printf("\nRight bottom y2:");
				max=dib.width-1;
				getint(&end_width,start_width,max);

				height=end_height-start_height+1;
				width=end_width-start_width+1;
				if((width*3)%4)
					padding=4-((width*3)%4);
			
				dib.bmp_bytes=(3*height*width)+(padding*height);

				buffer=(unsigned char*)malloc(dib.bmp_bytes);
				if(buffer==NULL)
				{
					printf("\nNo memory allocated");
					break;
				}
				crop(&dib,bitmapData,buffer,start_height,start_width,end_height,end_width);
				
				dib.height=-height;
				dib.width=width;
				bmp.size=dib.bmp_bytes+54;
	
				check=file_write(bmp,dib,buffer);
				if(!check)
				{
					printf("\nFile not return correctly");
					return 0;
				}
				if(bitmapData)
				{
					free(bitmapData);
					bitmapData=NULL;
				}
				break;
			case ZOOM:
				bitmapData = load_bmp(argv[1],&dib,&bmp);
				if(!bitmapData)
				{
					return 0;
				}
				printf("\nEnter the position:\nHeight:");
				max=-(dib.height)-1;
				getint(&row,CROP_MIN,max);

				printf("\nWidth:");
				max=dib.width-1;
				getint(&col,CROP_MIN,max);
				
				original_padding=(dib.bmp_bytes-(dib.width*(-dib.height)*3))/(-dib.height);
				original_height=-(dib.height);
				original_width=dib.width;
			
				printf("Enter the zoom factor:");
				getint(&zoom,ZOOM_MIN,ZOOM_MAX);
				
				width=(zoom*(dib.width-1)+1);
				height=-dib.height;
			
				dib.bmp_bytes=(3*height*width);

				buffer=(unsigned char*) malloc(dib.bmp_bytes);
				if(!buffer)
				{
					printf("\nNo memory allocated");
					break;
				}
				buffer=zoom_width(&dib,bitmapData,buffer,zoom,original_padding);
		
				dib.width=width; 
				height=(zoom*(height-1)+1);
				if((width*3)%4)
					padding=4-((width*3)%4);
		
				dib.bmp_bytes=(3*height*width)+(padding*height);
				zoom_buffer=(unsigned char*) malloc(dib.bmp_bytes);
				if(!zoom_buffer)
				{
					printf("\nMemory not allocated");
					break;
				}
					
				zoom_buffer=zoom_height(&dib,buffer,zoom_buffer,zoom);

				dib.height=-height;
				bmp.size=dib.bmp_bytes+54;
				
				start_height=(row*zoom)-(original_height/2);
				start_width=(col*zoom)-(original_width/2);
				end_height=(row*zoom)+(original_height/2)-1;
				end_width=(col*zoom)+(original_width/2)-1;
			
				if(start_height<0)
				{
					end_height=end_height-start_height+(row*zoom);
					start_height=(row*zoom);
				}
				else if(end_height>height-1)
				{
					start_height=start_height-(end_height-height+1);
					end_height=height-1;
				}
				if(start_width<0)
				{
					end_width=end_width-start_width+(col*zoom);
					start_width=(col*zoom);
				}
				else if(end_width>width-1)
				{
					start_width=start_width-(end_width-width+1);
					end_width=width-1;
				}
				
				dib.bmp_bytes=(3*original_height*original_width);
				result_buffer=(unsigned char*) malloc(dib.bmp_bytes);

				crop(&dib,zoom_buffer,result_buffer,start_height,start_width,end_height,end_width);
				bmp.size=dib.bmp_bytes+54;
				dib.height=-original_height;
				dib.width=original_width;

				check=file_write(bmp,dib,result_buffer);

				if(!check)
				{
					printf("File not return correctly");
					return 0;
				}
				if(zoom_buffer)
				{
					free(zoom_buffer);
					zoom_buffer=NULL;
				}
				if(buffer)
				{
					free(buffer);
					buffer=NULL;
				}
				if(bitmapData)
				{
					free(bitmapData);
					bitmapData=NULL;
				}
				break;
				
			case EXIT:
				if(bitmapData)
				{
					free(bitmapData);
					bitmapData=NULL;
				}
				return 0;
		}
	}while(1);

	
}