#include<stdio.h>
#include<stdlib.h>

enum choice{INSERT=1,DELETE,EXIT};
#define MIN 1
#define MAX 3

 /************************************************************************************************************
  *  Name	:	clbuf
  *  Arguments  :       No arguments
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to clear input buffer in case of 
                            input buffering.
  ************************************************************************************************************/
void clbuf() 
{
    while ( getchar()!= '\n');
}

 /************************************************************************************************************
  *  Name	:	getint
  *  Arguments  :       input as pointer,minimum input value,maximum input value
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to get integer input and to validate them.
  ************************************************************************************************************/

void getint(int *ref,int min,int max)
{
	while(!(scanf("%d",ref)) || (*ref<min || *ref>max))
	{
		clbuf();
		printf("Invalid input\nEnter a valid number: ");
	}
	clbuf();
}

/************************************************************************************************************
                            Declaration of structure 
  ************************************************************************************************************/

struct Node{
	int element;
	struct Node *left;
	struct Node *right;
};

 /************************************************************************************************************
  *  Name	:	max
  *  Arguments  :       integer a and integer b
  *  Returns	:	returns the maximum integer
  *  Description	:   This function is used to find the maximum integer among the two values.
  ************************************************************************************************************/

int max(int a,int b)
{
	return a>b? a: b;
}

/************************************************************************************************************
  *  Name	:	depth
  *  Arguments  :       structure pointer
  *  Returns	:	returns integer value
  *  Description	:   This function is used to find the depth of a particular node.
  ************************************************************************************************************/

int depth(struct Node* node)  
{ 
   if (node==NULL)  
       return 0; 
   else 
   { 
       int left_depth = depth(node->left); 
       int right_depth = depth(node->right); 
  
       if (left_depth > right_depth)  
           return(left_depth+1); 
       else return(right_depth+1); 
   } 
}


/************************************************************************************************************
  *  Name	:	newNode
  *  Arguments  :       integer element
  *  Returns	:	returns struct pointer
  *  Description	:   This function is used to create a new node and to save the element value in the node.
  ************************************************************************************************************/
struct Node *newNode(int element)
{
	struct Node *new=(struct Node*)malloc(sizeof(struct Node));
	if(new==NULL)
	{
		printf("No memory allocated");
		return NULL;
	}	
	new->left=NULL;
	new->right=NULL;
	new->element=element;
	return(new);
};


/************************************************************************************************************
  *  Name	:	rotate_right
  *  Arguments  :       structure pointer
  *  Returns	:	returns struct pointer
  *  Description	:   This function is used to rotate the tree towards right to balance it
  ************************************************************************************************************/
struct Node *rotate_right(struct Node *center)
{
	struct Node *temp1=center->left;
	struct Node *temp2=temp1->right;
	temp1->right=center;
	center->left=temp2;
	return temp1;
}

/************************************************************************************************************
  *  Name	:	rotate_left
  *  Arguments  :       structure pointer
  *  Returns	:	returns struct pointer
  *  Description	:   This function is used to rotate the tree towards left to balance it
  ************************************************************************************************************/
struct Node *rotate_left(struct Node *center)
{
	struct Node *temp1=center->right;
	struct Node *temp2=temp1->left;
	temp1->left=center;
	center->right=temp2;
	return temp1;
}


/************************************************************************************************************
  *  Name	:	find_balance
  *  Arguments  :       structure pointer
  *  Returns	:	returns integer value
  *  Description	:   This function is used to find if a node is balanced or not
  ************************************************************************************************************/

int find_balance(struct Node *node)
{
	if(node==NULL)
		return 0;
	return depth(node->left)-depth(node->right);
}

/************************************************************************************************************
  *  Name	:	balance
  *  Arguments  :       structure pointer
  *  Returns	:	returns struct pointer
  *  Description	:   This function is used to balance the tree 
  ************************************************************************************************************/

struct Node* balance(struct Node *node)
{
	int bal=find_balance(node);
	
	if(bal>1)
	{
		if(find_balance(node->left)>=0)
			return rotate_right(node);
		else if(find_balance(node->left)<0)
		{
			node->left=rotate_left(node->left);
			return rotate_right(node);
		}
	}
	else if(bal<-1)
	{
		if(find_balance(node->right)<=0)
			return rotate_left(node);
		else if(find_balance(node->right)>0)
		{
			node->right=rotate_right(node->right);
			return rotate_left(node);
		}
	}
	return node;
}




/************************************************************************************************************
  *  Name	:	insert
  *  Arguments  :       structure pointer and integer element
  *  Returns	:	returns struct pointer
  *  Description	:   This function is used to insert an element at a particular node in the tree
			     and to balance the tree 
  ************************************************************************************************************/
struct Node* insert(struct Node* node,int element)
{
	if(node==NULL)
	{
		return (newNode(element));
	}
	if(element < node->element)
		node->left = insert(node->left,element);
	else if(element > node->element)
		node->right=insert(node->right,element);
	else
		return node;
	
	node=balance(node);
	
	return node;
}



/************************************************************************************************************
  *  Name	:	delete
  *  Arguments  :       structure pointer and integer element
  *  Returns	:	returns struct pointer
  *  Description	:   This function is used to delete an element from a particular node in the tree
			     and to balance the tree 
  ************************************************************************************************************/

struct Node* delete(struct Node* node, int element)
{
	if(node==NULL)
		return node;
	if(element < (node->element))
		node->left = delete(node->left,element);
	else if(element > (node->element))
		node->right=delete(node->right,element);
	else
	{
		struct Node *temp=NULL;
		if(node->right==NULL || node->left==NULL)
		{
			if(node->left !=NULL)
				temp=node->left;
			else if(node->right !=NULL)
				temp=node->right;
			if(temp==NULL)
			{
				temp=node;
				node=NULL;
			}
			else
				*node=*temp;
			if(temp)
			{
				free(temp);
				temp=NULL;
			}
		}
		else
		{
			struct Node* temp1=node->right;
			while(temp1->left != NULL)
				temp1=temp1->left;
			node->element=temp1->element;
			node->right=delete(node->right,temp1->element);
		}
	}
	if(node==NULL)
		return node;

	node=balance(node);
	return node;
} 


/************************************************************************************************************
  *  Name	:	printlevel
  *  Arguments  :       structure pointer and integer level
  *  Returns	:	no return type
  *  Description	:   This function is used to print the elements in a level in the tree
  ***********************************************************************************************************/

void printlevel(struct Node* root, int level)
{
	int heigh=depth(root),index;
	if(root == NULL)	
	{
		printf("  ");
		return;
	}
	if(level==1)
	{
		for(index=1;index<=heigh;index++)
		printf(" ");
		printf("%d",root->element);
		for(index=1;index<=heigh;index++)
		printf(" ");
		heigh--;
	}
	else if(level>1)
	{
		printlevel(root->left,level-1);
		printlevel(root->right,level-1);
	}
}


/************************************************************************************************************
  *  Name	:	levelorder
  *  Arguments  :       structure pointer 
  *  Returns	:	no return type
  *  Description	:   This function is used to print the tree in level order manner
  ***********************************************************************************************************/
void levelorder(struct Node *root) 
{ 
    	int heigh=depth(root);
	for(int index=1;index<=heigh;index++)
	{
		printf("\n");
		printlevel(root,index);	
	}
} 

/************************************************************************************************************
  *  Name	:	free_tree
  *  Arguments  :       structure pointer 
  *  Returns	:	no return type
  *  Description	:   This function is used to deallocate memory
  ***********************************************************************************************************/

void free_tree(struct Node *node)
{
       	if (node != NULL) 
	{
		if(node->right)
		{
       			free_tree(node->right);
			node->right=NULL;
		}
		if(node->left)
		{
       			free_tree(node->left);
			node->left=NULL;
		}
        	free(node);
		node=NULL;
	}
}

int main()
{
	struct Node *root=NULL;
	int choice,element=0;
	do{
		printf("\nMenu:\n1.Insert\n2.Delete\n3.Exit\n");
		printf("Enter your choice:");
		getint(&choice,MIN,MAX);
		switch(choice)
		{
			case INSERT:
				printf("\nEnter the element:");
				getint(&element,MIN,INT_MAX);
				root=insert(root,element);
				levelorder(root);
				break;
			case DELETE:
				printf("\nEnter the element to be deleted:");
				getint(&element,MIN,INT_MAX); 
				root=delete(root,element);
				levelorder(root);
				break;
			case EXIT:
				levelorder(root);
				free_tree(root);
				return 0;
		}
	}while(1);
			
}


