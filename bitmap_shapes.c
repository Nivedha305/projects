#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

enum choice{RECTANGLE=1,SQUARE,CIRCLE,PARELLELOGRAM,DIAMOND};
enum color{RED=1,BLUE,GREEN,WHITE};
#define MAGIC_ARRAY_SIZE 2
#define HEADER_SIZE 54
#define BMP_HEADER_SIZE 40
#define BIT_COUNT 24
#define BPP 3
#define MIN 1
#define MAX 5
#define LEVEL_MAX 10
#define COLOR_MAX 4
#define WIDTH_LEVEL 100
#define PADDING 4
#define ARROW_WIDTH 80

/************************************************************************************************************
                            Declaration of array for 'BM' in bmp header
  ************************************************************************************************************/

unsigned char magic[2];

/************************************************************************************************************
                            Declaration of structure for bmp header
  ************************************************************************************************************/

typedef struct
{
    	uint32_t size; 
    	uint16_t reserved1;  
    	uint16_t reserved2;  
    	uint32_t offset;
  
}bmpheader;

/************************************************************************************************************
                            Declaration of structure for dib header
  ************************************************************************************************************/

typedef struct 
{
    	uint32_t header_sz; 
    	uint32_t width;  
    	uint32_t height;  
    	uint16_t nplanes; 
    	uint16_t bitcount; 
    	uint32_t compression;
    	uint32_t bmp_bytes;  
    	uint32_t hres;  
    	uint32_t vres;  
    	uint32_t ncolors;  
    	uint32_t nimpcolors; 

}dibheader;

/************************************************************************************************************
                            Declaration of structure for shapes
  ************************************************************************************************************/

struct shapes
{
	int color;
	int shape;
	int height;
	int width;
};	

/************************************************************************************************************
                            Declaration of structure for colors
  ************************************************************************************************************/

struct colors
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

/************************************************************************************************************
  *  Name	:	clbuf
  *  Arguments  :       No arguments
  *  Returns	:	Does not return anything
  *  Description	:   This function is clear buffer and is used to clear input buffer in case of 
                            input buffering.
  ************************************************************************************************************/

void clbuf()
{
	while(getchar()!='\n');
}

/************************************************************************************************************
  *  Name	:	getint
  *  Arguments  :       int pointer,minimum value,maximum value
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to get integer input and to validate them.
  ************************************************************************************************************/

void getint(int *ref,int min,int max)
{
	while(!(scanf("%d",ref)) || (*ref<min || *ref>max))
	{
	        clbuf();
		printf("Invalid input\nEnter a valid num: ");
	} 
	clbuf();
}

/************************************************************************************************************
  *  Name	:	write_bmpdata
  *  Arguments  :       dibheader pointer,bmpheader pointer,unsinged char buffer
  *  Returns	:	returns integer
  *  Description	:   This function is used to write the output buffer data to the bmp file
  ************************************************************************************************************/

int write_bmpdata(dibheader *dib,bmpheader *bmp,unsigned char* buffer)
{
	int size;

	FILE *fp1=fopen("test.bmp","wb");
	if (fp1 == NULL)
    	{
		printf("File does not open");	
        	return 0;
    	}
	
	magic[0]='B';
	magic[1]='M';
	size=fwrite(magic,sizeof(char),MAGIC_ARRAY_SIZE,fp1);              			//writing BM in the header to the file          
	if(size!=MAGIC_ARRAY_SIZE)
	{
		return 0;
	} 
	
	bmp->size=HEADER_SIZE+(BPP*(-dib->height)*(dib->width));
	bmp->reserved1=0;
	bmp->reserved2=0;
	bmp->offset=HEADER_SIZE;
	dib->header_sz=BMP_HEADER_SIZE;
	dib->nplanes=1;
	dib->bitcount=BIT_COUNT;
	dib->compression=0;
	dib->hres=0;
	dib->vres=0;
	dib->ncolors=0;
	dib->nimpcolors=0; 

	printf("width:%d\n", dib->width);
	printf("height:%d\n", dib->height);
	
	size=fwrite(bmp,sizeof(bmpheader),1,fp1);						//writing bmp header into the file		
	if(!size)
	{
		return 0;
	} 

	size=fwrite(dib,sizeof(dibheader),1,fp1);						//writing dib header into the file
	if(!size)
	{
		return 0;
	}
	
	fseek(fp1, bmp->offset, SEEK_SET);
			
	size=fwrite(buffer,dib->bmp_bytes,1,fp1);						//writing image data into the file
	if(!size)
	{
		return 0;
	} 
	
	fclose(fp1);
	if(buffer)
	{
		free(buffer);
		buffer=NULL;
	}
	return 1;
}

/************************************************************************************************************
  *  Name	:	get color
  *  Arguments  :     	integer
  *  Returns	:	returns struct variable
  *  Description	:   This function is used to find the values of r,g,b
  ************************************************************************************************************/

struct colors get_color(int color)
{
	struct colors ptr;
	switch(color)
	{
		case RED:
			ptr.r=255;					//rgb value for red
			ptr.g=0;
			ptr.b=0;
			break;
		case GREEN:
			ptr.r=0;					//rgb value for green
			ptr.g=255;
			ptr.b=0;
			break;
		case BLUE:
			ptr.r=0;					//rgb value for blue
			ptr.g=0;	
			ptr.b=255;
			break;
		case WHITE:
			ptr.r=255;					//rgb value for white
			ptr.g=255;
			ptr.b=255;
			break;
	}
	return ptr;
}

/************************************************************************************************************
  *  Name	:	rectangle
  *  Arguments  :       unsinged char buffer,struct pointer,integer height,integer width
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to fill the buffer with the rectangle image data
  ************************************************************************************************************/

unsigned char* rectangle(unsigned char* buffer,struct shapes *sptr,int height,int width,int padding)
{
	int row=0,col=0;

	int length=sptr->height;
	int breadth=sptr->width;

	int color=sptr->color;
	struct colors ptr;
	ptr=get_color(color);

	int start_width=(width/2)-(breadth/2);
	int end_width=start_width+breadth;

	for(row=height;row<height+length;row++)
	{
		for(col=start_width;col<end_width;col++)
		{
			buffer[((width*BPP)+padding) * row + col*BPP]=(ptr.b);
			buffer[((width*BPP)+padding) * row + col*BPP+1]=(ptr.g);
			buffer[((width*BPP)+padding) * row + col*BPP+2]=(ptr.r);
		}
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	circle
  *  Arguments  :       unsinged char buffer,struct pointer,integer height,integer width
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to fill the buffer with the circle image data
  ************************************************************************************************************/

unsigned char* circle(unsigned char* buffer,struct shapes* sptr,int height,int width,int padding)
{
	int row=0,col=0;

	int radius=(sptr->height)/2;

	int color=sptr->color;
	struct colors ptr;
	ptr=get_color(color);

	int center_height=height+radius;
	int center_width=(width/2);

	for(row=height;row<height+(sptr->height);row++)
	{
		for(col=0;col<width;col++)
		{
			if((((col-center_width)*(col-center_width))+((row-center_height)*(row-center_height))) <= (radius*radius))
			{
				buffer[((width*BPP)+padding) * row + col*BPP]=(ptr.b);
				buffer[((width*BPP)+padding) * row + col*BPP+1]=(ptr.g);
				buffer[((width*BPP)+padding) * row + col*BPP+2]=(ptr.r);
			}
		}
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	parellelogram
  *  Arguments  :       unsinged char buffer,struct pointer,integer height,integer width
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to fill the buffer with the parellelogram image data
  ************************************************************************************************************/

unsigned char* parellelogram(unsigned char* buffer,struct shapes* sptr,int height,int width,int padding)
{
	int row=0,col=0;

	int length=sptr->height;
	int breadth=sptr->width-(length-1);					//original breadth of parellelogram

	int color=sptr->color;
	struct colors ptr;
	ptr=get_color(color);

	int start_width=(width/2)-(breadth/2)+(length/2);			
	int end_width=start_width+breadth;

	for(row=height;row<height+length;row++)
	{
		for(col=start_width;col<end_width;col++)
		{
			buffer[((width*BPP)+padding) * row + col*BPP]=(ptr.b);
			buffer[((width*BPP)+padding) * row + col*BPP+1]=(ptr.g);
			buffer[((width*BPP)+padding) * row + col*BPP+2]=(ptr.r);
		}
		start_width--;
		end_width--;
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	diamond
  *  Arguments  :       unsinged char buffer,struct pointer,integer height,integer width
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to fill the buffer with the diamond image data
  ************************************************************************************************************/

unsigned char* diamond(unsigned char* buffer,struct shapes* sptr,int height,int width,int padding)
{
	int row=0,col=0;

	int length=sptr->height;

	int end_height=(height+length);
	int center=(width/2),diff=0;							//origin x point of circle
	int center_length=height+(length/2);						//origin y point of circle
	
	int color=sptr->color;
	struct colors ptr;
	ptr=get_color(color);

	for(row=height;row<end_height;row++)
	{
		for(col=center-diff;col<=center+diff;col++)
		{
			buffer[((width*BPP)+padding) * row + col*BPP]=(ptr.b);
			buffer[((width*BPP)+padding) * row + col*BPP+1]=(ptr.g);
			buffer[((width*BPP)+padding) * row + col*BPP+2]=(ptr.r);
		}
		if(row<center_length)
			diff++;
		else
			diff--;
	}	
	return buffer;
}

/************************************************************************************************************
  *  Name	:	arrow
  *  Arguments  :       unsinged char buffer,integer start_height,integer height_min,integer width
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to fill the buffer with the arrow image data
  ************************************************************************************************************/

unsigned char* arrow(unsigned char* buffer,int start_height,int height_min,int width,int padding)
{
	int row=0,col=0;
	int arrow_width=(width/ARROW_WIDTH);
	int start_width=(width/2)-arrow_width;   					//start width of line in arrow
	int end_width=(width/2)+arrow_width;						//end width of line in arrow
	int end_height=start_height+height_min;
	int arrow_height=end_height-(4*arrow_width);					//start height of traingle in arrow

	for(row=start_height;row<end_height;row++)
	{
		for(col=0;col<width;col++)
		{
			if(col>=start_width && col<=end_width)
			{
				buffer[((width*BPP)+padding) * row + col*BPP]=255;
				buffer[((width*BPP)+padding) * row + col*BPP+1]=255;
				buffer[((width*BPP)+padding) * row + col*BPP+2]=255;
			}
		}
		if(row==arrow_height)
		{
			start_width-=(2*arrow_width);					//decreasing start width by twice the width of the line
			end_width+=(2*arrow_width);					//decreasing end width by twice the width of the line
		}
		if(row>arrow_height)
		{
			start_width++;
			end_width--;
		}
	}
	return buffer;
}

int main()
{
	bmpheader bmp;
	dibheader dib;
	struct shapes *sptr=NULL,*ptr;
	int level=0,check=0,choice=0,height_max=0,width_max=0,index=0,height_min=INT_MAX,height=0,width=0,start_height=0,padding=0;
	unsigned char* buffer=NULL;
	printf("\nEnter the number of levels:");
	getint(&level,MIN,LEVEL_MAX);
	sptr=(struct shapes*)malloc(level*sizeof(struct shapes));

	for(index=0;index<level;index++)
	{
		ptr=(sptr+index);
		printf("\nChoose the shape:\n1.Rectangle\n2.Square\n3.Circle\n4.Parallelogram\n5.Diamond");
		printf("\nEnter your choice:");
		getint(&choice,MIN,MAX);
		switch(choice)
		{
			case RECTANGLE:
				ptr->shape=RECTANGLE;

				printf("\nEnter the length:");
				getint(&(ptr->height),MIN,INT_MAX);

				printf("\nEnter the width:");
				getint(&(ptr->width),MIN,INT_MAX);

				break;

			case SQUARE:
				ptr->shape=SQUARE;

				printf("\nEnter the length of side:");
				getint(&(ptr->height),MIN,INT_MAX);

				ptr->width=ptr->height;

				break;	

			case CIRCLE:
				ptr->shape=CIRCLE;

				printf("\nEnter the radius:");
				getint(&(ptr->height),MIN,INT_MAX);

				ptr->height=(ptr->height)*2;
				ptr->width=ptr->height;

				break;

			case PARELLELOGRAM:
				ptr->shape=PARELLELOGRAM;

				printf("\nEnter the length:");
				getint(&(ptr->height),MIN,INT_MAX);

				printf("\nEnter the width:");
				getint(&(ptr->width),MIN,INT_MAX);

				ptr->width=ptr->width+((ptr->height)-1);

				break;

			case DIAMOND:
				ptr->shape=DIAMOND;

				printf("\nEnter the length of side:");
				getint(&(ptr->height),MIN,INT_MAX);

				ptr->width=ptr->height;

				break;	

		}
		printf("\nChoose the color:\n1.Red\n2.Blue\n3.Green\n4.White");
		printf("\nEnter your choice:");
		getint(&(ptr->color),MIN,COLOR_MAX);	
		
		height_max+=ptr->height;
		if((ptr->height)<height_min)
			height_min=ptr->height;
		if((ptr->width)>width_max)
			width_max=ptr->width;
	}

	height=height_max+((level+1)*height_min);
	width=width_max+WIDTH_LEVEL;
		
	if((width*BPP)%PADDING)
		padding=PADDING-((width*BPP)%PADDING);
		
	dib.bmp_bytes=(BPP*height*width)+(padding*height);
	
	dib.height=-height;
	dib.width=width;

	buffer=(unsigned char*)calloc(dib.bmp_bytes,sizeof(unsigned char));
	if(!buffer)
	{
		printf("\nMemory not allocated");
		return 0;
	} 

	start_height+=height_min;
	
	for(index=0;index<level;index++)
	{
		if(index)
		{
			buffer=arrow(buffer,start_height,height_min,width,padding);
			start_height+=height_min;
		}
		ptr=(sptr+index);
		choice=ptr->shape;

		switch(choice)
		{
			case RECTANGLE:
				buffer=rectangle(buffer,ptr,start_height,dib.width,padding);
				start_height+=(ptr->height);
				break;

			case SQUARE:
				buffer=rectangle(buffer,ptr,start_height,dib.width,padding);
				start_height+=(ptr->height);
				break;	

			case CIRCLE:
				buffer=circle(buffer,ptr,start_height,dib.width,padding);
				start_height+=(ptr->height);
				break;

			case PARELLELOGRAM:
				buffer=parellelogram(buffer,ptr,start_height,dib.width,padding);
				start_height+=(ptr->height);
				break;

			case DIAMOND:
				buffer=diamond(buffer,ptr,start_height,dib.width,padding);
				start_height+=(ptr->height);
				break;

		}
	}

	check=write_bmpdata(&dib,&bmp,buffer);
	if(!check)
	{
		printf("\nFile not written correctly");
		return 0;
	} 
	if(sptr)
	{
		free(sptr);
		sptr=NULL;
	}

}