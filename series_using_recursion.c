#include <stdio.h>

void clbuf() 
{
    while (getchar() != '\n');
};

void getint(int *ref)
{
	printf("\nEnter the number: ");
	while(!(scanf("%d",ref)))
	{
		clbuf();
		printf("Invalid input\nEnter the number: ");
	}
}

int recursion(int first,int second,int third,int number)
{	
	if(number==1)
	     return first;
        else if(number==2)
	     return second;
        else if(number==3)
	     return third;
	return recursion(second,third,(first+second+third),number-1);
}

int main()
{
	int first=0,second=0,third=0,number;
	getint(&first);
	getint(&second);
	getint(&third);
	getint(&number);
	printf("\nThe %d element of series is:%d",number,recursion(first,second,third,number));
}