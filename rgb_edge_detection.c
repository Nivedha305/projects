#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<math.h>

#define MAGIC_ARRAY_SIZE 2
#define MIN 1
#define COLOR_MAX 4
#define BPP 3
enum color{RED=1,BLUE,GREEN,WHITE};

/************************************************************************************************************
  *  Name	:	clbuf
  *  Arguments  :       No arguments
  *  Returns	:	Does not return anything
  *  Description	:   This function is clear buffer and is used to clear input buffer in case of 
                            input buffering.
  ************************************************************************************************************/

void clbuf()
{
	while(getchar()!='\n');
}

/************************************************************************************************************
  *  Name	:	getint
  *  Arguments  :       int pointer,minimum value,maximum value
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to get integer input and to validate them.
  ************************************************************************************************************/

void getint(int *ref,int min,int max)
{
	while(!(scanf("%d",ref)) || (*ref<min || *ref>max))
	{
	        clbuf();
		printf("Invalid input\nEnter a valid num: ");
	} 
	clbuf();
}

/************************************************************************************************************
                            Declaration of array for 'BM' in bmp header
  ************************************************************************************************************/

unsigned char magic[2];

/************************************************************************************************************
                            Declaration of structure for bmp header
  ************************************************************************************************************/

typedef struct
{
    	uint32_t size; 
    	uint16_t reserved1;  
    	uint16_t reserved2;  
    	uint32_t offset; 
}bmpheader;

/************************************************************************************************************
                            Declaration of structure for dib header
  ************************************************************************************************************/

typedef struct 
{
    	uint32_t header_sz; 
    	uint32_t width;  
    	uint32_t height;  
    	uint16_t nplanes; 
    	uint16_t bitcount; 
    	uint32_t compression;
    	uint32_t bmp_bytes;  
    	uint32_t hres;  
    	uint32_t vres;  
    	uint32_t ncolors;  
    	uint32_t nimpcolors; 

}dibheader;

struct colors
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

struct colors get_color(int color)
{
	struct colors ptr;
	switch(color)
	{
		case RED:
			ptr.r=255;					//rgb value for red
			ptr.g=0;
			ptr.b=0;
			break;
		case GREEN:
			ptr.r=0;					//rgb value for green
			ptr.g=255;
			ptr.b=0;
			break;
		case BLUE:
			ptr.r=0;					//rgb value for blue
			ptr.g=0;	
			ptr.b=255;
			break;
		case WHITE:
			ptr.r=255;					//rgb value for blue
			ptr.g=255;	
			ptr.b=255;
			break;
	}
	return ptr;
}

/************************************************************************************************************
  *  Name	:	str_len
  *  Arguments  :       char* string
  *  Returns	:	returns integer
  *  Description	:   This function is used to find the length of the string
  ************************************************************************************************************/

int str_len(char *str)
{
	if(str==NULL)
		return 0;
    	int index;
    	for(index=0;str[index]!='\0';index++);
    	return index;
}

/************************************************************************************************************
  *  Name	:	filecheck_bmp
  *  Arguments  :       char pointer
  *  Returns	:	returns integer
  *  Description	:   This function is used to validate bmp file name
  ************************************************************************************************************/

int filecheck_bmp(char* cptr)
{
	int len=str_len(cptr);
	if(cptr[len-1]=='p' && cptr[len-2]=='m' && cptr[len-3]=='b' && cptr[len-4]=='.')		//check for .bmp file
		return 1;
	else 
		return 0;
}

/************************************************************************************************************
  *  Name	:	load_bmp
  *  Arguments  :       filename,dib header,bmp header
  *  Returns	:	returns the bitmapdata
  *  Description	:   This function is used to load the bmp file.
  ************************************************************************************************************/	

unsigned char *load_bmp(char *filename, dibheader *dib,bmpheader *bmp)
{
    	FILE *fp;
    	unsigned char *bitmapImage=NULL; 
	int size;

    	fp = fopen(filename,"rb");
    	if (fp == NULL)
    	{
		printf("\nFile does not open");	
		fclose(fp);
        	return NULL;
    	}
    	size=fread(&magic,sizeof(char),MAGIC_ARRAY_SIZE,fp);			//read BM from the bmp file header
	if(size!=MAGIC_ARRAY_SIZE)
	{
		printf("\nFile not read correctly");
		fclose(fp);	
        	return NULL;
	}
	if(magic[0]!='B' || magic[1]!='M')
	{
		printf("\nFile not read correctly");
		fclose(fp);	
        	return NULL;
	}

    	size=fread(bmp,1, sizeof(bmpheader),fp);				//read bmp header from file
	if(size!=sizeof(bmpheader))
	{
		printf("\nFile not read correctly");
		fclose(fp);
		return NULL;
	}

    	size=fread(dib,1, sizeof(dibheader),fp); 				//read dibheader from the file
	if(size!=sizeof(dibheader))
	{
		printf("\nFile not read correctly");
		fclose(fp);
		return NULL;
	}

    	fseek(fp, bmp->offset, SEEK_SET);              		      	//move the pointer to the start of image data

	bitmapImage = (unsigned char*)malloc(dib->bmp_bytes);         	//allocate memory for the size of image data
	if(!bitmapImage)
	{
		printf("\nNo memory allocated");
		fclose(fp);
		return NULL;
	}

    	fread(bitmapImage,dib->bmp_bytes,1,fp);				//read bitmapImage data from the file

    	if (bitmapImage == NULL)
    	{
		printf("\nFile not read correctly");
        	fclose(fp);
        	return NULL;
    	}

    	fclose(fp);
    	return bitmapImage;
}

/************************************************************************************************************
  *  Name	:	file_write
  *  Arguments  :       unsinged char buffer
  *  Returns	:	returns integer
  *  Description	:   This function is used to write the output buffer data to the bmp file
  ************************************************************************************************************/

int file_write(bmpheader bmp,dibheader dib,unsigned char* buffer)
{
	int size,i,count=0;
	FILE *fp1=fopen("test.bmp","wb");
	unsigned char* color_palette=NULL;

	if (fp1 == NULL)
    	{
		printf("File does not open");	
        	return 0;
    	}
	
	size=fwrite(magic,sizeof(char),MAGIC_ARRAY_SIZE,fp1);                          //writing BM data into ouput file
	if(size!=MAGIC_ARRAY_SIZE)
	{
		return 0;
	} 
	
	size=fwrite(&bmp,1,sizeof(bmpheader),fp1);			//writing bmp header data into ouput file
	if(size!=sizeof(bmpheader))
	{
		return 0;
	} 
	
	size=fwrite(&dib,1,sizeof(dibheader),fp1);			//writing dib header data into ouput file
	if(size!=sizeof(dibheader))
	{
		return 0;
	}

	fseek(fp1, bmp.offset, SEEK_SET);				//moving the pointer to start of image data
	
	size=fwrite(buffer,1,dib.bmp_bytes,fp1);			//writing image data into ouput file
	if(size!=dib.bmp_bytes)
	{
		return 0;
	} 

	fclose(fp1);
	if(buffer)
	{
		free(buffer);
		buffer=NULL;
	}
	return 1;
}

/************************************************************************************************************
  *  Name	:	intensity
  *  Arguments  :       dibheader pointer,unsigned char data,integer row,integer column
  *  Returns	:	returns integer
  *  Description	:   This function is used to find intensity of particular pixel value
  ************************************************************************************************************/

int intensity(unsigned char* data,int row,int col,dibheader dib)
{
	int width=dib.width,b,g,r,intensity;
	b=data[(width * row + col)*BPP];
	g=data[(width * row + col)*BPP+1];
	r=data[(width * row + col)*BPP+2];
	intensity=(0.3*r)+(0.59*g)+(0.11*b); 					//find intensity of a pixel
	return intensity;
}

/************************************************************************************************************
  *  Name	:	sobel
  *  Arguments  :       dibheader pointer,unsigned char data,unsigned char result
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to implement sobel algorithm to find the edges
  ************************************************************************************************************/

unsigned char* sobel(unsigned char* data,unsigned char* res,dibheader dib)
{
	int sobelx[3][3] = {-1, 0, 1, -10, 0, 10, -1, 0, 1}, sobely[3][3] = {1, 10, 1, 0, 0, 0, -1, -10, -1};   //defining sobel horizontal(x) and vertical(y) kernel matrix	
	int row=0,col=0,dx=0,dy=0,index;
	int width=dib.width;
	int height=dib.height;
	if(height<0)
		height=-height;
	for(col=0;col<width;col++)
	{
		res[col*BPP]=data[col*BPP];										//writing first row to result
		res[col*BPP+1]=data[col*BPP+1];
		res[col*BPP+2]=data[col*BPP+2];
		res[((width*(height-1))+col)*BPP]=data[((width*(height-1))+col)*BPP];				//writing last row to result						
		res[((width*(height-1))+col)*BPP+1]=data[((width*(height-1))+col)*BPP+1];
		res[((width*(height-1))+col)*BPP+2]=data[((width*(height-1))+col)*BPP+2];
	}
	for(row=1;row<height-1;row++)
	{
		res[(width*row)*BPP]=data[(width*row)*BPP];								//writing first column to result
		res[(width*row)*BPP+1]=data[(width*row)*BPP+1];
		res[(width*row)*BPP+2]=data[(width*row)*BPP+2];
		for(col=1;col<width-1;col++)
		{
			dx= (intensity(data,row-1,col-1,dib))*sobelx[0][0] + (intensity(data,row-1,col,dib))*sobelx[0][1] + (intensity(data,row-1,col+1,dib))*sobelx[0][2] 
				+ (intensity(data,row,col-1,dib))*sobelx[1][0] + (intensity(data,row,col,dib))*sobelx[1][1] + (intensity(data,row,col+1,dib))*sobelx[1][2]
				+ (intensity(data,row+1,col-1,dib))*sobelx[2][0] + (intensity(data,row+1,col,dib))*sobelx[2][1] + (intensity(data,row+1,col+1,dib))*sobelx[2][2];

			dy= (intensity(data,row-1,col-1,dib))*sobely[0][0] +  (intensity(data,row-1,col,dib))*sobely[0][1] + (intensity(data,row-1,col+1,dib))*sobely[0][2] 
				+ (intensity(data,row,col-1,dib))*sobely[1][0] + (intensity(data,row,col,dib))*sobely[1][1] +  (intensity(data,row,col+1,dib))*sobely[1][2]
				+ (intensity(data,row+1,col-1,dib))*sobely[2][0] + (intensity(data,row+1,col,dib))*sobely[2][1] + (intensity(data,row+1,col+1,dib))*sobely[2][2];
			double d=sqrt((dx*dx)+(dy*dy));
			
			if(d>255)
			{
				res[(width*row+col)*BPP]=0;
				res[(width*row+col)*BPP+1]=0;
				res[(width*row+col)*BPP+2]=0;
			}	
			else
			{
				res[(width*row+col)*BPP]=255;
				res[(width*row+col)*BPP+1]=255;
				res[(width*row+col)*BPP+2]=255;
			}
		}
		res[(width*row+col)*BPP]=data[(width*row+col)*BPP];						//writing last column to result
		res[(width*row+col)*BPP+1]=data[(width*row+col)*BPP+1];
		res[(width*row+col)*BPP+2]=data[(width*row+col)*BPP+2];
	}
	return res;
}

/************************************************************************************************************
  *  Name	:	check_boundary
  *  Arguments  :       dibheader pointer,unsigned char data,integer row,integer column
  *  Returns	:	returns integer
  *  Description	:   This function is used to find if a given pixel exists within the edges
  ************************************************************************************************************/

int check_boundary(unsigned char* data,int row,int col,dibheader dib)
{
	int height=dib.height;
	if(height<0)
		height=-height;
	int width=dib.width;
	int x,y;
	for(y=1;y<col;y++)
	{
		if(!data[(width*row+y)*BPP])
			break;
	}
	if(y==col)
		return 0;
	for(y=col+1;y<width-1;y++)
	{
		if(!data[(width*row+y)*BPP])
			break;
	}
	if(y==width-1)
		return 0;
	for(x=1;x<row;x++)
	{
		if(!data[(width*x+col)*BPP])
			break;
	}
	if(x==row)
		return 0;
	for(x=row+1;x<height-1;x++)
	{
		if(!data[(width*x+col)*BPP])
			break;
	}
	if(x==height-1)
		return 0;
	return 1;
}

/************************************************************************************************************
  *  Name	:	addColor
  *  Arguments  :       dibheader pointer,unsigned char data,integer color
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to add color to the pixels within the edges
  ************************************************************************************************************/

unsigned char* addColor(unsigned char* data, int color, dibheader dib)
{
	int row=0,col=0;
	int height=dib.height;
	if(height<0)
		height=-height;
	int width=dib.width;
	struct colors clr;
	clr=get_color(color);
	for(row=0;row<height;row++)
	{
		for(col=0;col<width;col++)
		{
			if(check_boundary(data,row,col,dib) && data[(width*row+col)*3])
			{
				data[(width*row+col)*BPP]=clr.b;
				data[(width*row+col)*BPP+1]=clr.g;
				data[(width*row+col)*BPP+2]=clr.r;
			}
		}
	}
	return data;
}

int main(int argc,char* argv[])
{
	if(argc==2)
		printf("\nThe file name is:%s",argv[1]);
	else if(argc>2)
	{
		printf("\nToo many arguments");
		return 0;
	}
	else
	{
		printf("\nOne argument expected");
		return 0;
	}
	if(!filecheck_bmp(argv[1]))
	{
		printf("\nInvalid file name");
		return 0;
	}

	bmpheader bmp;
	dibheader dib;
	int size=0,check=0,color=0;
	unsigned char *bitmapData=NULL,*res=NULL;
	bitmapData = load_bmp(argv[1],&dib,&bmp);
	if(!bitmapData)
	{
		return 0;
	}
	
	printf("\nChoose the color:\n1.Red\n2.Blue\n3.Green\n4.White");
	printf("\nEnter your choice:");
	getint(&color,MIN,COLOR_MAX);

	res =(unsigned char*)malloc(dib.bmp_bytes);
	if(!res)
	{
		printf("\nNo memory allocated");
		return 0;
	}
	
	res=sobel(bitmapData,res,dib);
	
	res=addColor(res,color,dib);
	
	check=file_write(bmp,dib,res);
	if(!check)
	{
		printf("\nFile not written properly");
	}
	if(bitmapData)
	{
		free(bitmapData);
		bitmapData=NULL;
	}
	printf("\nColor has been changed successfully");
}