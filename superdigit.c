#include<stdio.h>

void clbuf() 
{
    while ( getchar()!= '\n');
}

void getint(int *ref)
{
	printf("\nEnter the number: ");
	while(!(scanf("%d",ref)))
	{
		clbuf();
		printf("Invalid input\nEnter the number: ");
	}
}

int recursion(int number)
{ 
     if(number<10) 
     	return number;
     number=(number%10)+recursion(number/10);   
     return recursion(number); 
}
     

int main()
{
    int number, frequency, sum=0;
    getint(&number);
    getint(&frequency);
    while(number>0)
    {
        sum=sum+(number%10);
        number=number/10;
    }
    sum=sum*frequency;
    printf("%d",recursion(sum));
}
