#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

enum rgb_choice{RGB_TO_YUV444=1,RGB_TO_YUV422,RGB_TO_YUV420,RGB_BACK};
enum yuv_choice{YUV444_TO_RGB=1,YUV422_TO_RGB,YUV420_TO_RGB,YUV_BACK};
enum yuv_yuv_choice{YUV444=1,YUV422,YUV420,YUV_TO_YUV_BACK};
enum yuv444{YUV444_TO_YUV422=1,YUV444_TO_YUV420,YUV444_BACK};
enum yuv422{YUV422_TO_YUV444=1,YUV422_TO_YUV420,YUV422_BACK};
enum yuv420{YUV420_TO_YUV444=1,YUV420_TO_YUV422,YUV420_BACK};
enum flag{YUV444_FLAG=1,YUV422_FLAG,YUV420_FLAG};
enum choice{RGB_TO_YUV=1,YUV_TO_RGB,YUV_TO_YUV,EXIT};
#define MAGIC_ARRAY_SIZE 2
#define MIN 1
#define MAX 4
#define WIDTH 1920
#define HEIGHT 1280
#define YUV_MAX 3
#define COLOR_MAX 255
#define COLOR_MIN 0
#define YUV444_BPP 3
#define YUV422_BPP 2
#define YUV420_BPP 1.5
#define RGB_BPP 3
#define HEADER_SIZE 54
#define BMP_HEADER_SIZE 40
#define BIT_COUNT 24

/************************************************************************************************************
                            Declaration of array for 'BM' in bmp header
  ************************************************************************************************************/

unsigned char magic[2];

/************************************************************************************************************
                            Declaration of structure for bmp header
  ************************************************************************************************************/

typedef struct
{
    	uint32_t size; 
    	uint16_t reserved1;  
    	uint16_t reserved2;  
    	uint32_t offset;
  
}bmpheader;

/************************************************************************************************************
                            Declaration of structure for dib header
  ************************************************************************************************************/

typedef struct 
{
    	uint32_t header_sz; 
    	uint32_t width;  
    	uint32_t height;  
    	uint16_t nplanes; 
    	uint16_t bitcount; 
    	uint32_t compression;
    	uint32_t bmp_bytes;  
    	uint32_t hres;  
    	uint32_t vres;  
    	uint32_t ncolors;  
    	uint32_t nimpcolors; 

}dibheader;

/************************************************************************************************************
  *  Name	:	clbuf
  *  Arguments  :       No arguments
  *  Returns	:	Does not return anything
  *  Description	:   This function is clear buffer and is used to clear input buffer in case of 
                            input buffering.
  ************************************************************************************************************/

void clbuf()
{
	while(getchar()!='\n');
}

/************************************************************************************************************
  *  Name	:	getint
  *  Arguments  :       int pointer,minimum value,maximum value
  *  Returns	:	Does not return anything
  *  Description	:   This function is used to get integer input and to validate them.
  ************************************************************************************************************/

void getint(int *ref,int min,int max)
{
	while(!(scanf("%d",ref)) || (*ref<min || *ref>max))
	{
	        clbuf();
		printf("Invalid input\nEnter a valid num: ");
	} 
	clbuf();
}

/************************************************************************************************************
  *  Name	:	getinput
  *  Arguments  :       int pointer
  *  Returns	:	Returns the char pointer
  *  Description	:   This function is used to get filename as input
  ************************************************************************************************************/

char* getinput()
{
	int len=0;
	char *cptr=NULL;
	char ch;
        cptr=(char*) malloc(sizeof(char));
	if(cptr==NULL)
        {
		printf("No memory allocated");
		return 0;
	}
        while(scanf("%c",&ch) && ch!='\n')
	{
		cptr[len]=ch;   
		len++; 
		cptr=(char*) realloc(cptr,sizeof(char)*len+1);                                               
	} 
	cptr[len]='\0';
	return cptr; 
}

/************************************************************************************************************
  *  Name	:	str_len
  *  Arguments  :       char* string
  *  Returns	:	returns integer
  *  Description	:   This function is used to find the length of the string
  ************************************************************************************************************/

int str_len(char *str)
{
	if(str==NULL)
		return 0;
    	int index;
    	for(index=0;str[index]!='\0';index++);
    	return index;
}

/************************************************************************************************************
  *  Name	:	filecheck_raw
  *  Arguments  :       char pointer
  *  Returns	:	returns integer
  *  Description	:   This function is used to validate raw file name
  ************************************************************************************************************/

int filecheck_raw(char* cptr)
{
	int len=str_len(cptr);
	if(cptr[len-1]=='w' && cptr[len-2]=='a' && cptr[len-3]=='r' && cptr[len-4]=='.')
		return 1;
	else 
		return 0;
}

/************************************************************************************************************
  *  Name	:	filecheck_bmp
  *  Arguments  :       char pointer
  *  Returns	:	returns integer
  *  Description	:   This function is used to validate bmp file name
  ************************************************************************************************************/

int filecheck_bmp(char* cptr)
{
	int len=str_len(cptr);
	if(cptr[len-1]=='p' && cptr[len-2]=='m' && cptr[len-3]=='b' && cptr[len-4]=='.')
		return 1;
	else 
		return 0;
}

/************************************************************************************************************
  *  Name	:	load_yuv
  *  Arguments  :       filename
  *  Returns	:	returns the yuvdata
  *  Description	:   This function is used to load the .raw file.
  ************************************************************************************************************/

unsigned char *load_yuv(char *filename,int flag)
{
	FILE *fp;
    	unsigned char *yuvImage=NULL; 
	int size=0,len=0;

    	fp = fopen(filename,"rb");
    	if (fp == NULL)
    	{
		printf("\nFile does not open");	
		fclose(fp);
        	return NULL;
    	}
	fseek(fp,0,SEEK_END);
	size=ftell(fp);						//finding the size of image data
	if(!size)
	{
		printf("No data found");
		return NULL;
	}
	if(flag==YUV444_FLAG && size!=YUV444_BPP*HEIGHT*WIDTH)
	{
		printf("\nInvalid file");
		fclose(fp);
		return NULL;
	}
	if(flag==YUV422_FLAG && size!=YUV422_BPP*HEIGHT*WIDTH)
	{
		printf("\nInvalid file");
		fclose(fp);
		return NULL;
	}
	if(flag==YUV420_FLAG && size!=YUV420_BPP*HEIGHT*WIDTH)
	{
		printf("\nInvalid file");
		fclose(fp);
		return NULL;
	}
	rewind(fp);

	yuvImage = (unsigned char*)malloc(size*sizeof(unsigned char*));         	
	if(!yuvImage)
	{
		printf("\nNo memory allocated");
		fclose(fp);
		return NULL;
	}

    	len=fread(yuvImage,sizeof(unsigned char),size,fp); 	//reading the image data
    	if (len!=size)
    	{
		printf("\nFile not read correctly");
        	fclose(fp);
        	return NULL;
    	} 

    	fclose(fp);
    	return yuvImage;
}

/************************************************************************************************************
  *  Name	:	load_bmp
  *  Arguments  :       filename,dib header,bmp header
  *  Returns	:	returns the bitmapdata
  *  Description	:   This function is used to load the bmp file.
  ************************************************************************************************************/	

unsigned char *load_bmp(char *filename, dibheader *dib,bmpheader *bmp)
{
    	FILE *fp;
    	unsigned char *bitmapImage=NULL; 
	int size;

    	fp = fopen(filename,"rb");
    	if (fp == NULL)
    	{
		printf("\nFile does not open");	
		fclose(fp);
        	return NULL;
    	}
    	size=fread(&magic,sizeof(char),MAGIC_ARRAY_SIZE,fp);				//reading BM from header
	if(size!=MAGIC_ARRAY_SIZE)
	{
		printf("\nFile not read correctly");
		fclose(fp);	
        	return NULL;
	}
	if(magic[0]!='B' || magic[1]!='M')
	{
		printf("\nFile not read correctly");
		fclose(fp);	
        	return NULL;
	}

    	size=fread(bmp,1, sizeof(bmpheader),fp);					//reading bmp header
	if(size!=sizeof(bmpheader))
	{
		printf("\nFile not read correctly");
		fclose(fp);
		return NULL;
	}

    	size=fread(dib,1, sizeof(dibheader),fp); 					//reading dib header
	if(size!=sizeof(dibheader))
	{
		printf("\nFile not read correctly");
		fclose(fp);
		return NULL;
	}

    	fseek(fp, bmp->offset, SEEK_SET);              		      	

	bitmapImage = (unsigned char*)malloc(dib->bmp_bytes);         			
	if(!bitmapImage)
	{
		printf("\nNo memory allocated");
		fclose(fp);
		return NULL;
	}

    	size=fread(bitmapImage,dib->bmp_bytes,1,fp);					//reading image data

    	if(!bitmapImage)
    	{
		printf("\nFile not read correctly");
        	fclose(fp);
        	return NULL;
    	}

    	fclose(fp);
    	return bitmapImage;
}

/************************************************************************************************************
  *  Name	:	file_write
  *  Arguments  :       dib header, data
  *  Returns	:	returns integer
  *  Description	:   This function is used to write the output buffer data to the .raw file
  ************************************************************************************************************/

int file_write(dibheader dib,unsigned char* newdata,int flag)
{
	int size;
	FILE *fp1;
	if(flag==YUV444_FLAG)
	{
		fp1=fopen("yuv444.raw","wb");
	}
	if(flag==YUV422_FLAG)
	{
		fp1=fopen("yuv422.raw","wb");
	}
	if(flag==YUV420_FLAG)
	{
		fp1=fopen("yuv420.raw","wb");
	}
	if (fp1 == NULL)
    	{
		printf("File does not open");	
        	return 0;
    	}
	
	size=fwrite(newdata,1,dib.bmp_bytes,fp1);					//writing image data into the file
	if(size!=dib.bmp_bytes)
	{
		return 0;
	} 

	fclose(fp1);
	if(newdata)
	{
		free(newdata);
		newdata=NULL;
	}
	return 1;
}

/************************************************************************************************************
  *  Name	:	file_write
  *  Arguments  :       unsinged char buffer
  *  Returns	:	returns integer
  *  Description	:   This function is used to write the output buffer data to the bmp file
  ************************************************************************************************************/

int write_bmpdata(unsigned char* buffer)
{
	int size;
	bmpheader bmp;
	dibheader dib;

	FILE *fp1=fopen("test.bmp","wb");
	if (fp1 == NULL)
    	{
		printf("File does not open");	
        	return 0;
    	}
	
	magic[0]='B';
	magic[1]='M';
	size=fwrite(magic,sizeof(char),MAGIC_ARRAY_SIZE,fp1);              			//writing BM in the header to the file          
	if(size!=MAGIC_ARRAY_SIZE)
	{
		return 0;
	} 
	
	bmp.size=HEADER_SIZE+(RGB_BPP*HEIGHT*WIDTH);
	bmp.reserved1=0;
	bmp.reserved2=0;
	bmp.offset=HEADER_SIZE;
	dib.header_sz=BMP_HEADER_SIZE;
	dib.width=WIDTH;
	dib.height=-HEIGHT;
	dib.nplanes=1;
	dib.bitcount=BIT_COUNT;
	dib.compression=0;
	dib.bmp_bytes=(RGB_BPP*HEIGHT*WIDTH);
	dib.hres=0;
	dib.vres=0;
	dib.ncolors=0;
	dib.nimpcolors=0;
	
	size=fwrite(&bmp,1,sizeof(bmpheader),fp1);						//writing bmp header into the file		
	if(size!=sizeof(bmpheader))
	{
		return 0;
	} 
	
	size=fwrite(&dib,1,sizeof(dibheader),fp1);						//writing dib header into the file
	if(size!=sizeof(dibheader))
	{
		return 0;
	}
	
	fseek(fp1, bmp.offset, SEEK_SET);
				
	size=fwrite(buffer,1,dib.bmp_bytes,fp1);						//writing image data into the file
	if(size!=dib.bmp_bytes)
	{
		return 0;
	} 

	fclose(fp1);
	if(buffer)
	{
		free(buffer);
		buffer=NULL;
	}
	return 1;
}

/************************************************************************************************************
  *  Name	:	calculate_y
  *  Arguments  :       integer r, integer g,integer b
  *  Returns	:	returns double value
  *  Description	:   This function is used to calculate y from r,g,b
  ************************************************************************************************************/

double calculate_y(int r,int g,int b)
{
	return (0.299*r)+(0.587*g)+(0.114*b);
}

/************************************************************************************************************
  *  Name	:	calculate_u
  *  Arguments  :       integer r, integer g,integer b
  *  Returns	:	returns double value
  *  Description	:   This function is used to calculate u from r,g,b
  ************************************************************************************************************/

double calculate_u(int r,int g,int b)
{
	return (-0.147*r)-(0.289*g)+(0.436*b)+128;
}

/************************************************************************************************************
  *  Name	:	calculate_v
  *  Arguments  :       integer r, integer g,integer b
  *  Returns	:	returns double value
  *  Description	:   This function is used to calculate v from r,g,b
  ************************************************************************************************************/

double calculate_v(int r,int g,int b)
{
	return (0.615*r)-(0.515*g)-(0.100*b)+128;
}

/************************************************************************************************************
  *  Name	:	clamp
  *  Arguments  :       integer value
  *  Returns	:	returns integer
  *  Description	:   This function is used to clamp the values to the limit 0 to 255
  ************************************************************************************************************/

int clamp(int value)
{
	if(value>COLOR_MAX)
		return COLOR_MAX;
	else if(value<COLOR_MIN)
		return COLOR_MIN;
	return value;
}

/************************************************************************************************************
  *  Name	:	calculate_r
  *  Arguments  :       integer y, integer u,integer v
  *  Returns	:	returns integer
  *  Description	:   This function is used to calculate r from y,u,v
  ************************************************************************************************************/

int calculate_r(int y,int u,int v)
{
	int r=((298*y)+(409*v))>>8;
	return clamp(r);
}


/************************************************************************************************************
  *  Name	:	calculate_g
  *  Arguments  :       integer y, integer u,integer v
  *  Returns	:	returns integer
  *  Description	:   This function is used to calculate g from y,u,v
  ************************************************************************************************************/

int calculate_g(int y,int u,int v)
{
	int g=((298*y)-(100*u)-(208*v))>>8;
	return clamp(g);
}

/************************************************************************************************************
  *  Name	:	calculate_b
  *  Arguments  :       integer y, integer u,integer v
  *  Returns	:	returns integer
  *  Description	:   This function is used to calculate b from y,u,v
  ************************************************************************************************************/
	
int calculate_b(int y,int u,int v)
{
	int b=((298*y)+(516*u))>>8;
	return clamp(b);
}

/************************************************************************************************************
  *  Name	:	rgb_to_yuv444
  *  Arguments  :       dibheader pointer,unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert rgb to yuv444
  ************************************************************************************************************/

unsigned char* rgb_to_yuv444(dibheader *dib,unsigned char* data,unsigned char* buffer)
{
	int height=-(dib->height);
	int width=dib->width;
	int b,g,r,count=0;
	for(int row=0;row<height;row++)
	{
		for(int col=0;col<width;col++)
		{
			b=data[(width * row +col)*3];
			g=data[(width * row +col)*3+1];
			r=data[(width * row +col)*3+2];

			buffer[(width * row +col)*3]=calculate_y(r,g,b);
			buffer[(width * row +col)*3+1]=calculate_u(r,g,b);
			buffer[(width * row +col)*3+2]=calculate_v(r,g,b);
		}
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	yuv444_to_rgb
  *  Arguments  :       unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert yuv444 to rgb
  ************************************************************************************************************/

unsigned char* yuv444_to_rgb(unsigned char* data,unsigned char* buffer)
{
	int y,u,v,r,g,b;	
	for(int row=0;row<HEIGHT;row++)
	{
		for(int col=0;col<WIDTH;col++)
		{
			y=data[(WIDTH * row +col)*3];
			u=data[(WIDTH * row +col)*3+1]-128;
			v=data[(WIDTH * row +col)*3+2]-128;

			buffer[(WIDTH * row +col)*3]=calculate_b(y,u,v);
			buffer[(WIDTH * row +col)*3+1]=calculate_g(y,u,v);
			buffer[(WIDTH * row +col)*3+2]=calculate_r(y,u,v);
		}
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	rgb_to_yuv422
  *  Arguments  :       dibheader pointer,unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert rgb to yuv422
  ************************************************************************************************************/

unsigned char* rgb_to_yuv422(dibheader *dib,unsigned char* data,unsigned char* buffer)
{
	int height=-(dib->height);
	int width=dib->width;
	int p1_b,p1_g,p1_r,p2_b,p2_g,p2_r,count=0;
	double u,v;
	for(int row=0;row<height;row++)
	{
		for(int col=0;col<width;col+=2)
		{
			p1_b=data[(width * row +col)*3];
			p1_g=data[(width * row +col)*3+1];
			p1_r=data[(width * row +col)*3+2];

			p2_b=data[(width * row +(col+1))*3];
			p2_g=data[(width * row +(col+1))*3+1];
			p2_r=data[(width * row +(col+1))*3+2];

			u=(calculate_u(p1_r,p1_g,p1_b)+calculate_u(p2_r,p2_g,p2_b))/2;
			v=(calculate_v(p1_r,p1_g,p1_b)+calculate_v(p2_r,p2_g,p2_b))/2;
	
			buffer[(width * row +col)*2]=calculate_y(p1_r,p1_g,p1_b);
			buffer[(width * row +(col+1))*2]=calculate_y(p2_r,p2_g,p2_b);

			buffer[(width * row +col)*2+1]=u;
			buffer[(width * row +(col+1))*2+1]=v; 
		}
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	yuv422_to_rgb
  *  Arguments  :       unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert yuv422 to rgb
  ************************************************************************************************************/

unsigned char* yuv422_to_rgb(unsigned char* data,unsigned char* buffer)
{
	int y1,u,v,y2,r,g,b,row=0,col=0;
	for(row=0;row<HEIGHT;row++)
	{
		for(col=0;col<WIDTH;col+=2)
		{
			y1=data[(WIDTH * row +col)*2];
			u=data[(WIDTH * row +col)*2+1]-128;
			y2=data[(WIDTH * row +(col+1))*2];
			v=data[(WIDTH * row +(col+1))*2+1]-128;
			
			buffer[(WIDTH * row +col)*3]=calculate_b(y1,u,v);
			buffer[(WIDTH * row +col)*3+1]=calculate_g(y1,u,v);
			buffer[(WIDTH * row +col)*3+2]=calculate_r(y1,u,v);
				
			buffer[(WIDTH * row +(col+1))*3]=calculate_b(y2,u,v);
			buffer[(WIDTH * row +(col+1))*3+1]=calculate_g(y2,u,v);
			buffer[(WIDTH * row +(col+1))*3+2]=calculate_r(y2,u,v);
		}
	}
	return buffer; 
} 

/************************************************************************************************************
  *  Name	:	rgb_to_yuv420
  *  Arguments  :       dibheader pointer,unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert rgb to yuv420
  ************************************************************************************************************/

unsigned char* rgb_to_yuv420(dibheader *dib,unsigned char* data,unsigned char* buffer)
{
	int height=-(dib->height);
	int width=dib->width;
	int b,g,r,row=0,col=0;
	int start_u=height*width;
	int start_v=start_u+(start_u/4);
	double u=0,v=0;
	for(row=0;row<height;row+=2)
	{
		for(col=0;col<width;col+=2)
		{
			u=0,v=0;
			for(int index=0;index<2;index++)
			{
				b=data[(width * row +(col+index))*3];
				g=data[(width * row +(col+index))*3+1];
				r=data[(width * row +(col+index))*3+2];
				buffer[width * row +(col+index)]=calculate_y(r,g,b);
				u=u+calculate_u(r,g,b);
				v=v+calculate_v(r,g,b);

				b=data[(width * (row+1) +(col+index))*3];
				g=data[(width * (row+1) +(col+index))*3+1];
				r=data[(width * (row+1) +(col+index))*3+2];
				buffer[width * (row+1) +(col+index)]=calculate_y(r,g,b);
				u=u+calculate_u(r,g,b);
				v=v+calculate_v(r,g,b);
			}
			buffer[start_u++]=u/4;
			buffer[start_v++]=v/4;
		} 
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	yuv420_to_rgb
  *  Arguments  :       unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert yuv420 to rgb
  ************************************************************************************************************/
unsigned char* yuv420_to_rgb(unsigned char* data,unsigned char* buffer)
{
	int b,g,r,row=0,col=0,u,v,y1,y2,y3,y4;
	int start_u=HEIGHT*WIDTH;
	int start_v=start_u+(start_u/4);
	for(row=0;row<HEIGHT;row+=2)
	{
		for(col=0;col<WIDTH;col+=2)
		{
			y1=data[WIDTH * row +col];
			y2=data[WIDTH * row +(col+1)];
			y3=data[WIDTH * (row+1) +col];
			y4=data[WIDTH * (row+1) +(col+1)];
			u=data[start_u++]-128;
			v=data[start_v++]-128;

			buffer[(WIDTH * row +col)*3]=calculate_b(y1,u,v);
			buffer[(WIDTH * row +col)*3+1]=calculate_g(y1,u,v);
			buffer[(WIDTH * row +col)*3+2]=calculate_r(y1,u,v);
			
			buffer[(WIDTH * row +(col+1))*3]=calculate_b(y2,u,v);
			buffer[(WIDTH * row +(col+1))*3+1]=calculate_g(y2,u,v);
			buffer[(WIDTH * row +(col+1))*3+2]=calculate_r(y2,u,v);

			buffer[(WIDTH * (row+1) +col)*3]=calculate_b(y3,u,v);
			buffer[(WIDTH * (row+1) +col)*3+1]=calculate_g(y3,u,v);
			buffer[(WIDTH * (row+1) +col)*3+2]=calculate_r(y3,u,v);

			buffer[(WIDTH * (row+1) +(col+1))*3]=calculate_b(y4,u,v);
			buffer[(WIDTH * (row+1) +(col+1))*3+1]=calculate_g(y4,u,v);
			buffer[(WIDTH * (row+1) +(col+1))*3+2]=calculate_r(y4,u,v);
			
		} 
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	yuv444_to_yuv422
  *  Arguments  :       unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert yuv444 to yuv422
  ************************************************************************************************************/

unsigned char* yuv444_to_yuv422(unsigned char* data,unsigned char* buffer)
{
	int y1,u1,v1,y2,u2,v2;	
	double u,v;
	for(int row=0;row<HEIGHT;row++)
	{
		for(int col=0;col<WIDTH;col+=2)
		{
			y1=data[(WIDTH * row +col)*3];
			u1=data[(WIDTH * row +col)*3+1];
			v1=data[(WIDTH * row +col)*3+2];

			y2=data[(WIDTH * row +(col+1))*3];
			u2=data[(WIDTH * row +(col+1))*3+1];
			v2=data[(WIDTH * row +(col+1))*3+2];

			buffer[(WIDTH * row +col)*2]=y1;
			buffer[(WIDTH * row +(col+1))*2]=y2;
		
			u=(u1+u2)/2;
			v=(v1+v2)/2;

			buffer[(WIDTH * row +col)*2+1]=u;
			buffer[(WIDTH * row +(col+1))*2+1]=v; 		
		}
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	yuv444_to_yuv420
  *  Arguments  :       unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert yuv444 to yuv420
  ************************************************************************************************************/

unsigned char* yuv444_to_yuv420(unsigned char* data,unsigned char* buffer)
{
	int y1,u1,v1,y2,u2,v2,row,col;	
	double u=0,v=0;
	int start_u=HEIGHT*WIDTH;
	int start_v=start_u+(start_u/4);
	for(row=0;row<HEIGHT;row+=2)
	{
		for(col=0;col<WIDTH;col+=2)
		{
			u=0,v=0;
			for(int index=0;index<2;index++)
			{
				y1=data[(WIDTH * row +(col+index))*3];
				u1=data[(WIDTH * row +(col+index))*3+1];
				v1=data[(WIDTH * row +(col+index))*3+2];
				buffer[WIDTH * row +(col+index)]=y1;
				u=u+u1;
				v=v+v1;

				y2=data[(WIDTH * (row+1) +(col+index))*3];
				u2=data[(WIDTH * (row+1) +(col+index))*3+1];
				v2=data[(WIDTH * (row+1) +(col+index))*3+2];
				buffer[WIDTH * (row+1) +(col+index)]=y2;
				u=u+u1;
				v=v+v1;
			}
			buffer[start_u++]=u/4;
			buffer[start_v++]=v/4;
		} 
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	yuv422_to_yuv444
  *  Arguments  :       unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert yuv422 to yuv444
  ************************************************************************************************************/

unsigned char* yuv422_to_yuv444(unsigned char* data,unsigned char* buffer)
{
	int y1,u,v,y2,row=0,col=0;
	for(row=0;row<HEIGHT;row++)
	{
		for(col=0;col<WIDTH;col+=2)
		{
			y1=data[(WIDTH * row +col)*2];
			u=data[(WIDTH * row +col)*2+1];
			y2=data[(WIDTH * row +(col+1))*2];
			v=data[(WIDTH * row +(col+1))*2+1];
			
			buffer[(WIDTH * row +col)*3]=y1;
			buffer[(WIDTH * row +col)*3+1]=u;
			buffer[(WIDTH * row +col)*3+2]=v;
				
			buffer[(WIDTH * row +(col+1))*3]=y2;
			buffer[(WIDTH * row +(col+1))*3+1]=u;
			buffer[(WIDTH * row +(col+1))*3+2]=v;
		}
	}
	return buffer; 
} 

/************************************************************************************************************
  *  Name	:	yuv422_to_yuv420
  *  Arguments  :       unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert yuv422 to yuv420
  ************************************************************************************************************/

unsigned char* yuv422_to_yuv420(unsigned char* data,unsigned char* buffer)
{
	int y1,u1,y2,v1,y3,u2,y4,v2,row,col;	
	double u=0,v=0;
	int start_u=HEIGHT*WIDTH;
	int start_v=start_u+(start_u/4);
	for(row=0;row<HEIGHT;row+=2)
	{
		for(col=0;col<WIDTH;col+=2)
		{
			y1=data[(WIDTH * row +col)*2];
			u1=data[(WIDTH * row +col)*2+1];
			y2=data[(WIDTH * row +(col+1))*2];
			v1=data[(WIDTH * row +(col+1))*2+1];

			y3=data[(WIDTH * (row+1) +col)*2];
			u2=data[(WIDTH * (row+1) +col)*2+1];
			y4=data[(WIDTH * (row+1) +(col+1))*2];
			v2=data[(WIDTH * (row+1) +(col+1))*2+1];
				
			buffer[WIDTH * row +col]=y1;
			buffer[WIDTH * row +(col+1)]=y2;
			buffer[WIDTH * (row+1) +col]=y3;
			buffer[WIDTH * (row+1) +(col+1)]=y4;
			
			buffer[start_u++]=(u1+u2)/2;
			buffer[start_v++]=(v1+v2)/2;
		}
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	yuv420_to_yuv444
  *  Arguments  :       unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert yuv420 to yuv444
  ************************************************************************************************************/

unsigned char* yuv420_to_yuv444(unsigned char* data,unsigned char* buffer)
{
	int b,g,r,row=0,col=0,u,v,y1,y2,y3,y4;
	int start_u=HEIGHT*WIDTH;
	int start_v=start_u+(start_u/4);
	for(row=0;row<HEIGHT;row+=2)
	{
		for(col=0;col<WIDTH;col+=2)
		{
			y1=data[WIDTH * row +col];
			y2=data[WIDTH * row +(col+1)];
			y3=data[WIDTH * (row+1) +col];
			y4=data[WIDTH * (row+1) +(col+1)];
			u=data[start_u++];
			v=data[start_v++];

			buffer[(WIDTH * row +col)*3]=y1;
			buffer[(WIDTH * row +col)*3+1]=u;
			buffer[(WIDTH * row +col)*3+2]=v;
			
			buffer[(WIDTH * row +(col+1))*3]=y2;
			buffer[(WIDTH * row +(col+1))*3+1]=u;
			buffer[(WIDTH * row +(col+1))*3+2]=v;

			buffer[(WIDTH * (row+1) +col)*3]=y3;
			buffer[(WIDTH * (row+1) +col)*3+1]=u;
			buffer[(WIDTH * (row+1) +col)*3+2]=v;

			buffer[(WIDTH * (row+1) +(col+1))*3]=y4;
			buffer[(WIDTH * (row+1) +(col+1))*3+1]=u;
			buffer[(WIDTH * (row+1) +(col+1))*3+2]=v;
			
		} 
	}
	return buffer;
}

/************************************************************************************************************
  *  Name	:	yuv420_to_yuv422
  *  Arguments  :       unsigned char data,unsigned char buffer
  *  Returns	:	returns unsigned char buffer
  *  Description	:   This function is used to convert yuv420 to yuv422
  ************************************************************************************************************/

unsigned char* yuv420_to_yuv422(unsigned char* data,unsigned char* buffer)
{
	int row=0,col=0,u,v,y1,y2,y3,y4;
	int start_u=HEIGHT*WIDTH;
	int start_v=start_u+(start_u/4);
	for(row=0;row<HEIGHT;row+=2)
	{
		for(col=0;col<WIDTH;col+=2)
		{
			y1=data[WIDTH * row +col];
			y2=data[WIDTH * row +(col+1)];
			y3=data[WIDTH * (row+1) +col];
			y4=data[WIDTH * (row+1) +(col+1)];
			u=data[start_u++];
			v=data[start_v++];

			buffer[(WIDTH * row +col)*2]=y1;
			buffer[(WIDTH * row +col)*2+1]=u;
			buffer[(WIDTH * row +(col+1))*2]=y2;
			buffer[(WIDTH * row +(col+1))*2+1]=v;

			buffer[(WIDTH * (row+1) +col)*2]=y3;
			buffer[(WIDTH * (row+1) +col)*2+1]=u;
			buffer[(WIDTH * (row+1) +(col+1))*2]=y4;
			buffer[(WIDTH * (row+1) +(col+1))*2+1]=v;
		} 
	}
	return buffer;
}

int main()
{
	bmpheader bmp;
	dibheader dib;
	unsigned char *bitmapData=NULL,*buffer=NULL,*yuvData=NULL;
	int choice,check=0,rgb_choice=0,yuv_choice=0,yuv_yuv_choice=0,file_size=0;
	char* cptr=NULL;
	do{
		printf("\nMenu:\n1.Rgb to yuv\n2.yuv to rgb\n3.yuv to yuv\n4.Exit");
		printf("\nEnter your choice:");
		getint(&choice,MIN,MAX);
		switch(choice)
		{
			case RGB_TO_YUV:
				do
				{
					printf("\nConvert rgb to:\n1.yuv444\n2.yuv422\n3.yuv420\n4.Back");
					printf("\nEnter your choice:");
					getint(&rgb_choice,MIN,MAX);
					

					switch(rgb_choice)
					{
						case RGB_TO_YUV444:
							printf("\nEnter the filename:");
							cptr=getinput();
							if(!filecheck_bmp(cptr))
							{
								printf("\nInvalid file name");
								break;
							}		
							bitmapData = load_bmp(cptr,&dib,&bmp);
							if(!bitmapData)
							{
								return 0;
							}
							buffer=(unsigned char*)malloc(dib.bmp_bytes);
							if(buffer==NULL)
							{
								printf("No memory allocated");
								break;
							}
							buffer=rgb_to_yuv444(&dib,bitmapData,buffer);
							check=file_write(dib,buffer,YUV444_FLAG);
							if(!check)
							{
								printf("\nFile not written correctly");
							}
							if(bitmapData)
							{
								free(bitmapData);
								bitmapData=NULL;
							}
							if(cptr)
							{
								free(cptr);		
								cptr=NULL;
							}
							break;
		
						case RGB_TO_YUV422:
							printf("\nEnter the filename:");
							cptr=getinput();
							if(!filecheck_bmp(cptr))
							{
								printf("\nInvalid file name");
								break;
							}		
							bitmapData = load_bmp(cptr,&dib,&bmp);
							if(!bitmapData)
							{
								return 0;
							}		
							dib.bmp_bytes=YUV422_BPP*(-dib.height)*dib.width;
							buffer=(unsigned char*)malloc(dib.bmp_bytes);
							if(buffer==NULL)
							{
								printf("No memory allocated");
								break;
							}
							buffer=rgb_to_yuv422(&dib,bitmapData,buffer);
							check=file_write(dib,buffer,YUV422_FLAG);
							if(!check)
							{
								printf("\nFile not written correctly");
							}
							if(bitmapData)
							{
								free(bitmapData);
								bitmapData=NULL;
							}
							if(cptr)
							{
								free(cptr);		
								cptr=NULL;
							}
							break;

						case RGB_TO_YUV420:
							printf("\nEnter the filename:");
							cptr=getinput();
							if(!filecheck_bmp(cptr))
							{
								printf("\nInvalid file name");
								break;
							}		
							bitmapData = load_bmp(cptr,&dib,&bmp);
							if(!bitmapData)
							{
								return 0;
							}		
							dib.bmp_bytes=YUV420_BPP*(-dib.height)*dib.width;

							buffer=(unsigned char*)malloc(dib.bmp_bytes);
							if(buffer==NULL)
							{
								printf("No memory allocated");
								break;
							}
							buffer=rgb_to_yuv420(&dib,bitmapData,buffer);
	
							check=file_write(dib,buffer,YUV420_FLAG);
							if(!check)
							{
								printf("\nFile not written correctly");
							}
							if(bitmapData)
							{
								free(bitmapData);
								bitmapData=NULL;
							}
							if(cptr)
							{
								free(cptr);		
								cptr=NULL;
							}
							break;
						case RGB_BACK:
							rgb_choice=0;
							break;
					}	
				}while(rgb_choice);
				break;
			
			case YUV_TO_RGB:
				do
				{
					printf("\nConvert to:\n1.yuv444 to rgb\n2.yuv422 to rgb\n3.yuv420 to rgb\n4.Back");
					printf("\nEnter your choice:");
					getint(&yuv_choice,MIN,MAX);
					switch(yuv_choice)
					{
						case YUV444_TO_RGB:
							printf("\nEnter the filename:");
							cptr=getinput();
							if(!filecheck_raw(cptr))
							{
								printf("\nInvalid file name");
								break;
							}
							yuvData = load_yuv(cptr,YUV444_FLAG);
							if(!yuvData)
							{
								break;
							}
							dib.bmp_bytes=RGB_BPP*HEIGHT*WIDTH;
							buffer=(unsigned char*)malloc(dib.bmp_bytes);
							if(buffer==NULL)
							{
								printf("No memory allocated");
								break;
							}
							buffer=yuv444_to_rgb(yuvData,buffer);
							check=write_bmpdata(buffer);
							if(!check)
							{
								printf("\nFile not written correctly");
							}
							if(yuvData)
							{
								free(yuvData);
								yuvData=NULL;
							}
							if(cptr)
							{
								free(cptr);
								cptr=NULL;
							}
							break;
		
						case YUV422_TO_RGB:
							printf("\nEnter the filename:");
							cptr=getinput();
							if(!filecheck_raw(cptr))
							{
								printf("\nInvalid file name");
								break;
							}
							yuvData = load_yuv(cptr,YUV422_FLAG);
							if(!yuvData)
							{
								break;
							}
							dib.bmp_bytes=RGB_BPP*HEIGHT*WIDTH;
							buffer=(unsigned char*)malloc(dib.bmp_bytes);
							if(buffer==NULL)
							{
								printf("No memory allocated");
								break;
							}
							buffer=yuv422_to_rgb(yuvData,buffer);
							check=write_bmpdata(buffer);
							if(!check)
							{
								printf("\nFile not written correctly");
							}
							if(yuvData)
							{
								free(yuvData);
								yuvData=NULL;
							}
							if(cptr)
							{
								free(cptr);
								cptr=NULL;
							}
							break;

						case YUV420_TO_RGB:
							printf("\nEnter the filename:");
							cptr=getinput();
							if(!filecheck_raw(cptr))
							{
								printf("\nInvalid file name");
								break;
							}
							yuvData = load_yuv(cptr,YUV420_FLAG);
							if(!yuvData)
							{
								break;
							}
							dib.bmp_bytes=RGB_BPP*HEIGHT*WIDTH;
							buffer=(unsigned char*)malloc(dib.bmp_bytes);
							if(buffer==NULL)
							{
								printf("No memory allocated");
								break;
							}
							buffer=yuv420_to_rgb(yuvData,buffer);
							
							check=write_bmpdata(buffer);
							if(!check)
							{
								printf("\nFile not written correctly");
							}
							if(yuvData)
							{
								free(yuvData);
								yuvData=NULL;
							}
							
							if(cptr)
							{
								free(cptr);
								cptr=NULL;
							}
							break;
						case YUV_BACK:
							yuv_choice=0;
							break;
					}	
				}while(yuv_choice);
				break;
				
			case YUV_TO_YUV:
				do
				{
					printf("\nConvert from:\n1.yuv444\n2.yuv422\n3.yuv420\n4.Back");
					printf("\nEnter your choice:");
					getint(&yuv_choice,MIN,MAX);
					switch(yuv_choice)
					{
						case YUV444:
							do
							{
								printf("\nConvert to:\n1.yuv422\n2.yuv420\n3.Back");
								printf("\nEnter your choice:");
								getint(&yuv_yuv_choice,MIN,YUV_MAX);
								switch(yuv_yuv_choice)
								{
									case YUV444_TO_YUV422:
										printf("\nEnter the filename:");
										cptr=getinput();
										if(!filecheck_raw(cptr))
										{
											printf("\nInvalid file name");
											break;
										}
										yuvData = load_yuv(cptr,YUV444_FLAG);
										if(!yuvData)
										{
											break;
										}
										dib.bmp_bytes=YUV422_BPP*HEIGHT*WIDTH;
									
										buffer=(unsigned char*)malloc(dib.bmp_bytes);
										if(buffer==NULL)		
										{
											printf("No memory allocated");
											return 0;
										}
										buffer=yuv444_to_yuv422(yuvData,buffer);
										check=file_write(dib,buffer,YUV422_FLAG);
										if(!check)
										{
											printf("\nFile not written correctly");
										}
										if(yuvData)
										{
											free(yuvData);
											yuvData=NULL;
										}
										
										if(cptr)
										{
											free(cptr);
											cptr=NULL;
										}
										break;
									case YUV444_TO_YUV420:
										printf("\nEnter the filename:");
										cptr=getinput();
										if(!filecheck_raw(cptr))
										{
											printf("\nInvalid file name");
											break;
										}
										yuvData = load_yuv(cptr,YUV444_FLAG);
						
										if(!yuvData)
										{
											break;
										}
										dib.bmp_bytes=YUV420_BPP*HEIGHT*WIDTH;
					
										buffer=(unsigned char*)malloc(dib.bmp_bytes);
										if(buffer==NULL)		
										{
											printf("No memory allocated");
											break;
										}
										buffer=yuv444_to_yuv420(yuvData,buffer);
										check=file_write(dib,buffer,YUV420_FLAG);
										if(!check)
										{
											printf("\nFile not written correctly");
										}
										if(yuvData)
										{
											free(yuvData);
											yuvData=NULL;
										}
										if(cptr)
										{
											free(cptr);
											cptr=NULL;
										}
										break;
									case YUV444_BACK:
										yuv_yuv_choice=0;
										break;
								}
							}while(yuv_yuv_choice);
							break;
		
						case YUV422:
							do
							{
								printf("\nConvert to:\n1.yuv444\n2.yuv420\n3.Back");
								printf("\nEnter your choice:");
								getint(&yuv_yuv_choice,MIN,YUV_MAX);
								switch(yuv_yuv_choice)
								{
									case YUV422_TO_YUV444:
										printf("\nEnter the filename:");
										cptr=getinput();
										if(!filecheck_raw(cptr))
										{
											printf("\nInvalid file name");
											break;
										}
										yuvData = load_yuv(cptr,YUV422_FLAG);
										if(!yuvData)
										{
											break;
										}
										dib.bmp_bytes=YUV444_BPP*HEIGHT*WIDTH;
										buffer=(unsigned char*)malloc(dib.bmp_bytes);
										if(buffer==NULL)		
										{
											printf("No memory allocated");
											break;
										}
										buffer=yuv422_to_yuv444(yuvData,buffer);
										check=file_write(dib,buffer,YUV444_FLAG);
										if(!check)
										{
											printf("\nFile not written correctly");
										}
										if(yuvData)
										{
											free(yuvData);
											yuvData=NULL;
										}
										if(cptr)
										{
											free(cptr);
											cptr=NULL;
										}
										break;
									case YUV422_TO_YUV420:
										printf("\nEnter the filename:");
										cptr=getinput();
										if(!filecheck_raw(cptr))
										{
											printf("\nInvalid file name");
											break;
										}
										yuvData = load_yuv(cptr,YUV422_FLAG);
										if(!yuvData)
										{
											break;
										}
										dib.bmp_bytes=YUV420_BPP*HEIGHT*WIDTH;
									
										buffer=(unsigned char*)malloc(dib.bmp_bytes);
										if(buffer==NULL)		
										{
											printf("No memory allocated");
											break;
										}
										buffer=yuv422_to_yuv420(yuvData,buffer);
										check=file_write(dib,buffer,YUV420_FLAG);
										if(!check)
										{
											printf("\nFile not written correctly");
										}
										if(yuvData)
										{
											free(yuvData);
											yuvData=NULL;
										}
										if(cptr)
										{
											free(cptr);
											cptr=NULL;
										}
										break;
									case YUV422_BACK:
										yuv_yuv_choice=0;
										break;
								}
							}while(yuv_yuv_choice);
							break;

						case YUV420:
							do
							{
								printf("\nConvert to:\n1.yuv444\n2.yuv422\n3.Back");
								printf("\nEnter your choice:");
								getint(&yuv_yuv_choice,MIN,YUV_MAX);
								switch(yuv_yuv_choice)
								{
									case YUV420_TO_YUV444:
										printf("\nEnter the filename:");
										cptr=getinput();
										if(!filecheck_raw(cptr))
										{
											printf("\nInvalid file name");
											break;
										}
										yuvData = load_yuv(cptr,YUV420_FLAG);
										if(!yuvData)
										{
											break;
										}
										dib.bmp_bytes=YUV444_BPP*HEIGHT*WIDTH;
										buffer=(unsigned char*)malloc(dib.bmp_bytes);
										if(buffer==NULL)		
										{
											printf("No memory allocated");
											break;
										}
										buffer=yuv420_to_yuv444(yuvData,buffer);
										check=file_write(dib,buffer,YUV444_FLAG);
										if(!check)
										{
											printf("\nFile not written correctly");
										}
										if(yuvData)
										{
											free(yuvData);
											yuvData=NULL;
										}
										if(cptr)
										{
											free(cptr);
											cptr=NULL;
										}
										break;
									case YUV420_TO_YUV422:
										printf("\nEnter the filename:");
										cptr=getinput();
										if(!filecheck_raw(cptr))
										{
											printf("\nInvalid file name");
											break;
										}
										yuvData = load_yuv(cptr,YUV420_FLAG);
										if(!yuvData)
										{
											break;
										}
										dib.bmp_bytes=YUV422_BPP*HEIGHT*WIDTH;
										
										buffer=(unsigned char*)malloc(dib.bmp_bytes);
										if(buffer==NULL)		
										{
											printf("No memory allocated");
											break;
										}
										buffer=yuv420_to_yuv422(yuvData,buffer);
										check=file_write(dib,buffer,YUV422_FLAG);
										if(!check)
										{
											printf("\nFile not written correctly");
										}
										if(yuvData)
										{
											free(yuvData);
											yuvData=NULL;
										}
										if(cptr)
										{
											free(cptr);
											cptr=NULL;
										}
										break;
									case YUV420_BACK:
										yuv_yuv_choice=0;
										break;
								}
							}while(yuv_yuv_choice);
							break;
						case YUV_TO_YUV_BACK:
							yuv_choice=0;
							break;
					}	
				}while(yuv_choice);
				break;
			case EXIT:
				if(bitmapData)
				{
					free(bitmapData);
					bitmapData=NULL;
				}
				if(yuvData)
				{
					free(yuvData);
					yuvData=NULL;
				}
				return 0;
		}
	}while(1);
}

